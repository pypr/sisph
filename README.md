# Simple Iterative Incompressible SPH

This repository contains the code and manuscript for research done on
the Simple Iterative Incompressible SPH scheme.

## File tree

The following are the contents of the of this repository. The `code/` directory
contains algorithms and examples that are used in the manuscript, the
`manuscript/` directory contains the LaTeX code for the generation of the
manuscript, the `automate.py` file runs all the test cases that are used in the
present study, the file `requirements.txt` specifies the dependencies that are
required to run the current code and the license is at `LICENSE.txt`.

```bash
.

|-- automate.py
|-- code
|   |-- bicgstab.py
|   |-- cavity.py
|   |-- dam_break_2d.py
|   |-- dam_break_3d.py
|   |-- elliptical_drop.py
|   |-- flow_past_cylinder.py
|   |-- mat_coeff.py
|   |-- mat_coeff.py.mako
|   |-- poiseuille.py
|   |-- ppe_test.py
|   |-- sisph_inlet_outlet.py
|   |-- sisph.py
|   |-- square_patch.py
|   |-- taylor_green.py
|   |-- try_wall_normal.py
|   |-- two_blocks.py
|   `-- wall_normal.py
|-- LICENSE.txt
|-- manuscript
|   |-- model6-num-names.bst
|   |-- paper.tex
|   `-- references.bib
|-- README.md
`-- requirements.txt
```

The algorithms mentioned in the manuscript are implemented in `sisph.py`, and
`wall_normal.py`. The `wall_normal.py` contains the algorithm to compute the
normals.

The following files contain the test problems used in the manuscript:
 - `cavity.py` - A two-dimensional lid driven cavity.
 - `dam_break_2d.py` - Two-dimensional dam break over a dry bed..
 - `dam_break_3d.py` - Three-dimensional Dam Break over a dry bed.
 - `elliptical_drop.py` - Evolution of a circular patch of incompressible fluid.
 - `flow_past_cylinder.py` - Incompressible flow past a circular cylinder.
 - `square_patch.py` - Evolution of a square patch of fluid under strain.
 - `taylor_green.py` - Taylor Green vortex flow.
 - `two_blocks.py` - Two square blocks of water colliding with each other.
 - `bicgstab.py` - A Bi-Conjugate Gradient Stabilized implementation.
 - `ppe_test.py` - A simple test to check if the PPE solver is working correctly.

The helper file `sisph_inlet_outlet.py` is used to implement the inlet/outlet
boundary conditions for flow past cylinder and `try_wall_normal.py` is a simple
example to test how the wall normals are computed.

## Installation

Running the code in this repository requires the Python programming language and
the PySPH package to be installed.  The following instructions provide broad
instructions on how to setup the software:

0. Setup a suitable Python distribution, using
   [edm](https://docs.enthought.com/edm/) or [conda](https://conda.io) or a
   [virtualenv](https://virtualenv.pypa.io/). The installation instructions are
   also available at https://pysph.readthedocs.io/en/latest/installation.html

1. If you have the source code of this repository as a compressed file then
   uncompress it, and move to step 2.

   (or)

   If you do not have the code, you can clone the git repository using:

        $ git clone https://gitlab.com/pypr/sisph.git

2. Then change directory to the uncompressed or cloned repository:

	     $ cd sisph

3. Run the following from your Python environment:

        $ pip install -r requirements.txt

## Running an example

A simple example to check if your installation proceeded as expected, run the
following command from the top level directory:

    $ python code/square_patch.py

it will take about 30 seconds on a single core CPU to run the above simulation and
the output will be generated in the current directory with a folder named
`square_patch_output`. A figure `final.png` will be generated in the output
folder.

## Generating the results

The paper and the results are all automated using the
[automan](https://automan.readthedocs.io) package which should already be
installed as part of the above installation. This will perform all the
required simulations (this can take a while) and also generate all the plots
for the manuscript.

To run all the simulations and generate the figures for the square patch
problem, do the following:

    $ python automate.py squarepatch

you can see all the test problems used in the manuscript using,

    $ python automate.py -h

To run all the simulations and generate all the figures, do the following:

    $ python automate.py

By default the simulation outputs are in the ``outputs`` directory and the
final plots for the paper are in ``manuscript/figures``.

NOTE: You need to have a GPU enabled machine to run `TaylorGreenPerf` and
`DamBreak3DPerf`. By default most of codes use OpenMP to run the simulations,
this can be changed by modifying the `backend` variable in `automate.py`, empty
string, `''`, indicates single core, the default value `' --openmp'` (mind the
empty space before `--openmp`) indicates multi-core using OpenMP, `' --opencl'`
indicates the use of GPU through OpenCL. You can also change `n_core` and
`n_thread` variables in `automate.py` to control the number of cores and number
of threads that are used while running with ` --openmp` backend.

## Building the paper

The manuscript is written with LaTeX and if you have that installed you may do
the following:

```
$ cd manuscript
$ pdflatex paper.tex
$ bibtex paper
$ pdflatex paper.tex
$ pdflatex paper.tex
```
