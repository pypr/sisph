from compyle.api import declare
from pysph.sph.equation import Equation

class ComputeAConjPerf(Equation):
    def initialize(self, d_idx, d_AConj, d_diag, d_Conj_hat):
        d_AConj[d_idx] = d_diag[d_idx]*d_Conj_hat[d_idx]

    def loop_all(self, d_idx, d_AConj, d_odiag_coeff, d_nbr_idx, d_Conj_hat,
                 d_diag, s_Conj_hat, NBRS, N_NBRS, d_offset, s_source_idx,
                 d_no_of_sources):

        j, idx, offset, nbr_idx = declare('int', 4)

        if s_source_idx[0] == 0:
            offset = 0
        else:
            offset = d_offset[d_idx*d_no_of_sources[0] + s_source_idx[0] - 1]
        for j in range(N_NBRS):
            idx = d_idx*100 + offset + j
            nbr_idx = d_nbr_idx[idx]
            d_AConj[d_idx] += d_odiag_coeff[idx] * (s_Conj_hat[nbr_idx])


class ComputeASPerf(Equation):
    def initialize(self, d_idx, d_AS, d_diag, d_S_hat):
        d_AS[d_idx] = d_diag[d_idx]*d_S_hat[d_idx]

    def loop_all(self, d_idx, d_AS, d_odiag_coeff, d_nbr_idx, d_S_hat,
                 d_diag, s_S_hat, NBRS, N_NBRS, d_offset, s_source_idx,
                 d_no_of_sources):

        j, idx, offset, nbr_idx = declare('int', 4)

        if s_source_idx[0] == 0:
            offset = 0
        else:
            offset = d_offset[d_idx*d_no_of_sources[0] + s_source_idx[0] - 1]
        for j in range(N_NBRS):
            idx = d_idx*100 + offset + j
            nbr_idx = d_nbr_idx[idx]
            d_AS[d_idx] += d_odiag_coeff[idx] * (s_S_hat[nbr_idx])



# What should be the sources here ?
# If yes what is the boundary condition?
class ComputeAConj(Equation):
    def __init__(self, dest, sources, rho0, rho_cutoff=0.8):
        self.rho0 = rho0
        self.rho_cutoff = rho_cutoff
        super(ComputeAConj, self).__init__(dest, sources)

    def initialize(self, d_idx, d_AConj, d_diag, d_Conj_hat):
        d_AConj[d_idx] = d_diag[d_idx]*d_Conj_hat[d_idx]

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho, d_diag, d_odiag_pk, s_pk,
             s_Conj_hat, d_AConj,
             XIJ, DWIJ, R2IJ, EPS):
        rhoij = (s_rho[s_idx] + d_rho[d_idx])
        rhoij2_1 = 1.0/(d_rho[d_idx]*rhoij)

        xdotdwij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]

        fac = 4.0 * s_m[s_idx] * rhoij2_1 * xdotdwij / (R2IJ + EPS)

        d_AConj[d_idx] += -fac * (s_Conj_hat[s_idx])

    def post_loop(self, d_idx, d_rho, d_AConj):
        if d_rho[d_idx] / self.rho0 < self.rho_cutoff:
            d_AConj[d_idx] = 0.0


# What should be the sources here ?
# If yes what is the boundary condition?
class ComputeAS(Equation):
    def __init__(self, dest, sources, rho0, rho_cutoff=0.8):
        self.rho0 = rho0
        self.rho_cutoff = rho_cutoff
        super(ComputeAS, self).__init__(dest, sources)

    def initialize(self, d_idx, d_AS, d_diag, d_S_hat):
        d_AS[d_idx] = d_diag[d_idx]*d_S_hat[d_idx]

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho, d_diag, d_odiag_pk, s_pk,
             s_S_hat, d_AS,
             XIJ, DWIJ, R2IJ, EPS):
        rhoij = (s_rho[s_idx] + d_rho[d_idx])
        rhoij2_1 = 1.0/(d_rho[d_idx]*rhoij)

        xdotdwij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]

        fac = 4.0 * s_m[s_idx] * rhoij2_1 * xdotdwij / (R2IJ + EPS)

        d_AS[d_idx] += -fac * (s_S_hat[s_idx])

    def post_loop(self, d_idx, d_rho, d_AS):
        if d_rho[d_idx] / self.rho0 < self.rho_cutoff:
            d_AS[d_idx] = 0.0



class ProjectConj(Equation):
    def initialize(self, d_idx, d_Conj):
        d_Conj[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_Conj, s_Conj, WIJ):
        d_Conj[d_idx] += s_Conj[s_idx]*WIJ

    def post_loop(self, d_idx, d_wij, d_Conj, d_rho, d_pk):
        if d_wij[d_idx] > 1e-14:
            d_Conj[d_idx] /= d_wij[d_idx]


class ProjectS(Equation):
    def initialize(self, d_idx, d_S):
        d_S[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_S, s_S, WIJ):
        d_S[d_idx] += s_S[s_idx]*WIJ

    def post_loop(self, d_idx, d_wij, d_S, d_rho, d_pk):
        if d_wij[d_idx] > 1e-14:
            d_S[d_idx] /= d_wij[d_idx]


