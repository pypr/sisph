"""
SISPH Inlet outlet
"""
# License: BSD

from pysph.sph.integrator_step import IntegratorStep
from pysph.sph.bc.inlet_outlet_manager import InletOutletManager
from sisph import PECIntegrator, SISPHScheme, PECIntegratorGTVF
from pysph.sph.equation import Group, Equation


class SISPHInletOutlet(InletOutletManager):
    def add_io_properties(self, pa, scheme=None):
        DEFAULT_PROPS = ['xn', 'yn', 'zn', 'ioid', 'disp',
                         'wij']
        for prop in DEFAULT_PROPS:
            pa.add_property(prop)

        pa.add_constant('pmin', 1000000.0)

    def get_stepper(self, scheme, cls):
        steppers = {}
        if (cls == PECIntegratorGTVF):
            if isinstance(scheme, SISPHScheme):
                for inlet in self.inlets:
                    steppers[inlet] = InletStepSISPH()
                for outlet in self.outlets:
                    steppers[outlet] = OutletStepSISPH()
                self.active_stages = [3]
        else:
            print("Using a different Integrator.")

        return steppers

    def get_equations(self, scheme=None, summation_density=False):
        from copy import deepcopy
        equations = []

        g00 = []
        for inlet in self.inletinfo:
            for eqn in inlet.equations:
                eq = deepcopy(eqn)
                g00.append(eq)
        for outlet in self.outletinfo:
            for eqn in outlet.equations:
                eq = deepcopy(eqn)
                g00.append(eq)

        if g00:
            equations.append(Group(equations=g00, real=False))

        return equations


class OutletStepSISPH(IntegratorStep):
    def initialize(self, d_x0, d_x, d_idx, d_uhat, d_uhat0, dt):
        d_x0[d_idx] = d_x[d_idx]

        d_uhat0[d_idx] = d_uhat[d_idx]

        d_x[d_idx] = d_x0[d_idx] + dt*d_uhat[d_idx]

    # Will only work if the Integrator has stage 3.
    def stage3(self, d_idx, d_x, d_x0, d_v, d_uhat, d_uhat0, dt):
        d_x[d_idx] = d_x0[d_idx] + 0.5*dt * (d_uhat0[d_idx] + d_uhat[d_idx])


class InletStepSISPH(IntegratorStep):
    def initialize(self, d_x0, d_idx, d_x, d_u, dt):
        d_x0[d_idx] = d_x[d_idx]
        d_x[d_idx] = d_x0[d_idx] + dt*d_u[d_idx]

    # Will only work if the Integrator has stage 3.
    def stage3(self, d_idx, d_x, d_x0, d_u, dt):
        d_x[d_idx] = d_x0[d_idx] + dt*d_u[d_idx]
