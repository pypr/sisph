from compyle.api import declare
from pysph.sph.equation import Equation

% for VAR in ('Conj', 'S'):
class ComputeA${VAR}Perf(Equation):
    def initialize(self, d_idx, d_A${VAR}, d_diag, d_${VAR}_hat):
        d_A${VAR}[d_idx] = d_diag[d_idx]*d_${VAR}_hat[d_idx]

    def loop_all(self, d_idx, d_A${VAR}, d_odiag_coeff, d_nbr_idx, d_${VAR}_hat,
                 d_diag, s_${VAR}_hat, NBRS, N_NBRS, d_offset, s_source_idx,
                 d_no_of_sources):

        j, idx, offset, nbr_idx = declare('int', 4)

        if s_source_idx[0] == 0:
            offset = 0
        else:
            offset = d_offset[d_idx*d_no_of_sources[0] + s_source_idx[0] - 1]
        for j in range(N_NBRS):
            idx = d_idx*100 + offset + j
            nbr_idx = d_nbr_idx[idx]
            d_A${VAR}[d_idx] += d_odiag_coeff[idx] * (s_${VAR}_hat[nbr_idx])


% endfor

% for VAR in ('Conj', 'S'):
# What should be the sources here ?
# If yes what is the boundary condition?
class ComputeA${VAR}(Equation):
    def __init__(self, dest, sources, rho0, rho_cutoff=0.8):
        self.rho0 = rho0
        self.rho_cutoff = rho_cutoff
        super(ComputeA${VAR}, self).__init__(dest, sources)

    def initialize(self, d_idx, d_A${VAR}, d_diag, d_${VAR}_hat):
        d_A${VAR}[d_idx] = d_diag[d_idx]*d_${VAR}_hat[d_idx]

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho, d_diag, d_odiag_pk, s_pk,
             s_${VAR}_hat, d_A${VAR},
             XIJ, DWIJ, R2IJ, EPS):
        rhoij = (s_rho[s_idx] + d_rho[d_idx])
        rhoij2_1 = 1.0/(d_rho[d_idx]*rhoij)

        xdotdwij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]

        fac = 4.0 * s_m[s_idx] * rhoij2_1 * xdotdwij / (R2IJ + EPS)

        d_A${VAR}[d_idx] += -fac * (s_${VAR}_hat[s_idx])

    def post_loop(self, d_idx, d_rho, d_A${VAR}):
        if d_rho[d_idx] / self.rho0 < self.rho_cutoff:
            d_A${VAR}[d_idx] = 0.0


% endfor

% for VAR in ('Conj', 'S'):
class Project${VAR}(Equation):
    def initialize(self, d_idx, d_${VAR}):
        d_${VAR}[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_${VAR}, s_${VAR}, WIJ):
        d_${VAR}[d_idx] += s_${VAR}[s_idx]*WIJ

    def post_loop(self, d_idx, d_wij, d_${VAR}, d_rho, d_pk):
        if d_wij[d_idx] > 1e-14:
            d_${VAR}[d_idx] /= d_wij[d_idx]


% endfor
