import numpy as np
from pysph.examples import dam_break_3d as DB
from pysph.sph.scheme import WCSPHScheme, SchemeChooser
from sisph import SISPHScheme


dim = 3
g = 9.81
tf = 6.0

# parameter to change the resolution
dx = 0.02
nboundary_layers = 4
hdx = 1.0
ro = 1000.0
h0 = dx * hdx
gamma = 7.0
alpha = 0.25
beta = 0.0


class DamBreak3D(DB.DamBreak3D):
    def initialize(self):
        self.vref = np.sqrt(2.0*g*0.55)
        self.co = 10*self.vref
        self.p0 = ro*self.co**2

    def consume_user_options(self):
        dx = self.options.dx
        self.dx = dx
        self.geom = DB.DamBreak3DGeometry(
            dx=dx, nboundary_layers=nboundary_layers, hdx=hdx, rho0=ro
        )
        self.vref = self.geom.get_max_speed(g=g)
        self.co = 10.0 * self.vref

    def create_particles(self):
        from compyle.api import use_config

        # Something about the particle creation is broken when the backend
        # is set to the GPU ones so we first do this on a CPU and
        # then update the backend.
        with use_config(use_opencl=False, use_cuda=False):
            [fluid, boundary, obstacle] = self.geom.create_particles()
            self.scheme.setup_properties([fluid, boundary, obstacle])

        for pa in [fluid, boundary, obstacle]:
            pa.update_backend()

        return [fluid, boundary]

    def configure_scheme(self):
        from pysph.base.kernels import QuinticSpline
        from pysph.sph.integrator import EPECIntegrator

        kernel = QuinticSpline(dim=dim)
        if self.options.scheme == 'sisph':
            dt = 0.125*h0/self.vref
            self.scheme.configure(c0=self.co)
        if self.options.scheme == 'wcsph':
            dt = 0.25*h0/(1.1*self.co)
            self.scheme.configure(c0=self.co)
            self.scheme.configure_solver(integrator_cls=EPECIntegrator)
        self.scheme.configure_solver(
            kernel=kernel, dt=dt, tf=tf, adaptive_timestep=False, pfreq=10,
            output_at_times=[0.4, 0.6, 1.0, 1.3, 1.5, 1.6, 1.8, 2.0, 2.5]
        )

    def create_scheme(self):
        cs = 10 * np.sqrt(2.0*g*0.55)
        sisph = SISPHScheme(
            fluids=['fluid'], solids=['boundary'], dim=dim, nu=0.0,
            c0=cs, rho0=ro, alpha=alpha, gz=-g, pref=self.p0,
            hg_correction=True, symmetric=True, gtvf=True, tolerance=1e-2,
            internal_flow=False
        )
        wcsph = WCSPHScheme(
            ['fluid'], ['boundary'], dim=dim, rho0=ro, c0=cs,
            h0=h0, hdx=hdx, gz=-9.81, alpha=alpha, beta=beta, gamma=gamma,
            hg_correction=True, tensile_correction=False
        )
        s = SchemeChooser(
            default='sisph', sisph=sisph, wcsph=wcsph
        )
        return s

    def customize_output(self):
        self._mayavi_config('''
        b = particle_arrays['fluid']
        b.scalar = 'vmag'
        t = particle_arrays['boundary']
        t.opacity = 0.1
        t.plot.actor.mapper.scalar_visibility = 0
        ''')


if __name__ == '__main__':
    app = DamBreak3D()
    app.run()
