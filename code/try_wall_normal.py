import matplotlib.pyplot as plt
import numpy as np
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Group

from wall_normal import ComputeNormals, SmoothNormals

nx = 51
x, y = np.mgrid[-1:1:nx*1j, -1:1:nx*1j]
mask = (x < -0.9) | (y < -0.9)
x = x[mask]
y = y[mask]
dx = 2/(nx - 1)
rho = 1.0
m = dx*dx*rho
pa = get_particle_array(name='solid', m=m, rho=rho, x=x, y=y, h=dx)
pa.add_property('normal', stride=3)
pa.add_property('normal_tmp', stride=3)

seval = SPHEvaluator(
    arrays=[pa], equations=[
        Group(equations=[ComputeNormals(dest='solid', sources=['solid'])]),
        Group(equations=[SmoothNormals(dest='solid', sources=['solid'])]),
    ],
    dim=2
)
seval.evaluate()


n = pa.normal
plt.scatter(pa.x, pa.y, marker='.')
plt.quiver(pa.x, pa.y, n[::3], n[1::3])
plt.axis('equal')
plt.show()
