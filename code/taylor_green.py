"""Taylor Green vortex flow (5 minutes).
"""
import numpy as np
from pysph.examples import taylor_green as TG

from sisph import SISPHScheme


U, L = TG.U, TG.L


class TaylorGreen(TG.TaylorGreen):

    def consume_user_options(self):
        nx = self.options.nx
        re = self.options.re
        self.nu = nu = U * L / re
        self.dx = dx = L / nx
        self.volume = dx * dx
        self.hdx = self.options.hdx

        h0 = self.hdx * self.dx
        dt_cfl = 0.25 * h0 / U
        dt_viscous = 0.125 * h0**2 / nu
        dt_force = 0.25 * 1.0

        self.dt = min(dt_cfl, dt_viscous, dt_force)
        self.tf = 5.0
        self.kernel_correction = self.options.kernel_correction
        # Trick parent class into working correctly when creating particles.
        self.options.scheme = 'sisph'

    def configure_scheme(self):
        scheme = self.scheme
        pfreq = 10
        scheme.configure(nu=self.nu)
        scheme.configure_solver(
            tf=self.tf, dt=self.dt, pfreq=pfreq,
            output_at_times=[0.2, 0.4, 0.8, 1.0]
        )

    def create_scheme(self):
        sisph = SISPHScheme(
            fluids=['fluid'], solids=[], dim=2, nu=None, rho0=TG.rho0,
            c0=TG.c0, alpha=0.0, has_ghosts=True, pref=TG.p0,
            rho_cutoff=0.2, internal_flow=True
        )
        return sisph

    def create_particles(self):
        [fluid] = super(TaylorGreen, self).create_particles()
        nfp = fluid.get_number_of_particles()
        fluid.gid[:] = np.arange(nfp)
        fluid.add_output_arrays(['gid'])
        return [fluid]

    def customize_output(self):
        self._mayavi_config('''
        b = particle_arrays['fluid']
        b.scalar = 'vmag'
        ''')


if __name__ == '__main__':
    app = TaylorGreen()
    app.run()
    app.post_process(app.info_filename)
