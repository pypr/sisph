"""
Incompressible SPH
"""
import numpy
from numpy import sqrt
from compyle.api import declare
import scipy.sparse
from scipy.sparse.linalg import bicgstab

from pysph.sph.scheme import Scheme, add_bool_argument
from pysph.base.utils import get_particle_array
from pysph.sph.integrator import Integrator
from pysph.sph.integrator_step import IntegratorStep
from pysph.sph.equation import Equation, Group, MultiStageEquations


def get_particle_array_sisph(constants=None, **props):
    sisph_props = [
        'u0', 'v0', 'w0', 'x0', 'y0', 'z0', 'rho0', 'diag', 'odiag_pk',
        'pk', 'rhs', 'pdiff', 'wg', 'vf', 'vg', 'ug', 'wij','wij2', 'wf', 'uf',
        'V', 'au', 'av', 'aw', 'dt_force', 'dt_cfl', 'vmag',
        'auhat', 'avhat', 'awhat', 'p0', 'uhat', 'vhat', 'what',
        'uhat0', 'vhat0', 'what0', 'pabs',

        'ug_star', 'vg_star', 'wg_star',

        # gtvf accelerations
        'dummy_uhat', 'dummy_vhat', 'dummy_what',
        'dummy_x', 'dummy_y', 'dummy_z',

        'kappa',

        # BiCGStab props
        'AX', 'precond', 'residue', 'q', 'Conj', 'AConj', 'S', 'AS',
        'residue_hat', 'Conj_hat', 'S_hat',
    ]

    pa = get_particle_array(
        additional_props=sisph_props, constants=constants, **props
    )

    pa.add_constant('iters', [0])
    pa.add_constant('pdiffk', numpy.zeros(30002))
    pa.add_constant('pmax', [0.0])
    pa.add_constant('kmax', [0.0])

    # Conjugate gradient props
    pa.add_constant('alpha', [0.0])
    pa.add_constant('beta', [0.0])
    pa.add_constant('omega', [0.0])
    pa.add_constant('res_norm', [0.0])

    # Odiag precompute, assuming neighbours are less than 100!
    pa.add_property('odiag_coeff', stride=100)
    pa.add_property('nbr_idx', stride=100, type='int')
    pa.add_property('ctr', type='int')

    pa.add_output_arrays(['p', 'V', 'vmag', 'p0', 'rhs', 'kappa', 'diag'])
    return pa


class PECIntegrator(Integrator):
    def one_timestep(self, t, dt):
        self.initialize()

        self.update_domain()

        self.compute_accelerations(0)

        self.stage1()

        self.update_domain()

        self.do_post_stage(0.5*dt, 1)

        self.compute_accelerations(1, update_nnps=False)

        self.stage2()

        self.update_domain()

        self.do_post_stage(dt, 2)

    def initial_acceleration(self, t, dt):
        pass


class PECIntegratorGTVF(Integrator):
    def one_timestep(self, t, dt):
        self.initialize()

        self.update_domain()

        self.compute_accelerations(0)

        self.stage1()

        self.do_post_stage(0.5*dt, 1)

        self.update_domain()

        self.compute_accelerations(1, update_nnps=False)

        self.stage2()

        self.do_post_stage(0.5*dt, 2)

        self.compute_accelerations(2, update_nnps=False)

        self.stage3()

        self.update_domain()

        self.do_post_stage(dt, 3)

    def initial_acceleration(self, t, dt):
        pass


class SISPHStep(IntegratorStep):
    def initialize(self, d_idx, d_x, d_y, d_z, d_x0, d_y0, d_z0, d_u, d_v,
                   d_w, d_u0, d_v0, d_w0, dt):
        d_x0[d_idx] = d_x[d_idx]
        d_y0[d_idx] = d_y[d_idx]
        d_z0[d_idx] = d_z[d_idx]

        d_u0[d_idx] = d_u[d_idx]
        d_v0[d_idx] = d_v[d_idx]
        d_w0[d_idx] = d_w[d_idx]

        d_x[d_idx] += dt*d_u[d_idx]
        d_y[d_idx] += dt*d_v[d_idx]
        d_z[d_idx] += dt*d_w[d_idx]

    def stage1(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, dt):
        d_u[d_idx] += dt*d_au[d_idx]
        d_v[d_idx] += dt*d_av[d_idx]
        d_w[d_idx] += dt*d_aw[d_idx]

    def stage2(self, d_idx, d_x, d_y, d_z, d_u, d_v, d_w, d_u0, d_v0, d_w0,
               d_x0, d_y0, d_z0, d_au, d_av, d_aw, d_vmag, d_dt_cfl,
               d_dt_force, dt):
        d_u[d_idx] += dt*d_au[d_idx]
        d_v[d_idx] += dt*d_av[d_idx]
        d_w[d_idx] += dt*d_aw[d_idx]

        d_x[d_idx] = d_x0[d_idx] + 0.5*dt * (d_u[d_idx] + d_u0[d_idx])
        d_y[d_idx] = d_y0[d_idx] + 0.5*dt * (d_v[d_idx] + d_v0[d_idx])
        d_z[d_idx] = d_z0[d_idx] + 0.5*dt * (d_w[d_idx] + d_w0[d_idx])

        d_vmag[d_idx] = sqrt(d_u[d_idx]*d_u[d_idx] + d_v[d_idx]*d_v[d_idx] +
                             d_w[d_idx]*d_w[d_idx])

        d_dt_cfl[d_idx] = 2.0*d_vmag[d_idx]

        au = (d_u[d_idx] - d_u0[d_idx])/dt
        av = (d_v[d_idx] - d_v0[d_idx])/dt
        aw = (d_w[d_idx] - d_w0[d_idx])/dt

        d_dt_force[d_idx] = 4.0*(au*au + av*av + aw*aw)


class SISPHGTVFStep(IntegratorStep):
    def initialize(self, d_idx, d_x, d_y, d_z, d_x0, d_y0, d_z0, d_u, d_v,
                   d_w, d_u0, d_v0, d_w0, d_uhat, d_vhat, d_what, d_uhat0,
                   d_vhat0, d_what0, dt):
        d_x0[d_idx] = d_x[d_idx]
        d_y0[d_idx] = d_y[d_idx]
        d_z0[d_idx] = d_z[d_idx]

        d_u0[d_idx] = d_u[d_idx]
        d_v0[d_idx] = d_v[d_idx]
        d_w0[d_idx] = d_w[d_idx]

        d_uhat0[d_idx] = d_uhat[d_idx]
        d_vhat0[d_idx] = d_vhat[d_idx]
        d_what0[d_idx] = d_what[d_idx]

        d_x[d_idx] += dt*d_uhat[d_idx]
        d_y[d_idx] += dt*d_vhat[d_idx]
        d_z[d_idx] += dt*d_what[d_idx]

    def stage1(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, dt):
        d_u[d_idx] += dt*d_au[d_idx]
        d_v[d_idx] += dt*d_av[d_idx]
        d_w[d_idx] += dt*d_aw[d_idx]

    def stage2(self, d_idx, d_x, d_y, d_z, d_u, d_v, d_w, d_x0, d_y0, d_z0,
               d_au, d_av, d_aw, d_uhat, d_vhat, d_what, d_auhat, d_avhat,
               d_awhat, d_uhat0, d_vhat0, d_what0, d_vmag, d_dt_cfl, dt,
               d_u0, d_v0, d_w0, d_dt_force):
        d_u[d_idx] += dt*d_au[d_idx]
        d_v[d_idx] += dt*d_av[d_idx]
        d_w[d_idx] += dt*d_aw[d_idx]

        d_vmag[d_idx] = sqrt(d_u[d_idx]*d_u[d_idx] + d_v[d_idx]*d_v[d_idx] +
                             d_w[d_idx]*d_w[d_idx])
        d_dt_cfl[d_idx] = 2.0*d_vmag[d_idx]

        au = (d_u[d_idx] - d_u0[d_idx])/dt
        av = (d_v[d_idx] - d_v0[d_idx])/dt
        aw = (d_w[d_idx] - d_w0[d_idx])/dt

        d_dt_force[d_idx] = 4*(au*au + av*av + aw*aw)
        d_dt_force[d_idx] = dt

    def stage3(self, d_idx, d_u, d_v, d_w, d_uhat, d_vhat, d_what,
               d_x0, d_y0, d_z0, d_uhat0, d_vhat0, d_what0,
               d_dummy_x, d_dummy_y, d_dummy_z,
               d_x, d_y, d_z, dt):
        d_uhat[d_idx] = d_u[d_idx] + d_dummy_x[d_idx]/dt
        d_vhat[d_idx] = d_v[d_idx] + d_dummy_y[d_idx]/dt
        d_what[d_idx] = d_w[d_idx] + d_dummy_z[d_idx]/dt

        d_x[d_idx] = d_x0[d_idx] + 0.5*dt * (d_uhat0[d_idx] + d_uhat[d_idx])
        d_y[d_idx] = d_y0[d_idx] + 0.5*dt * (d_vhat0[d_idx] + d_vhat[d_idx])
        d_z[d_idx] = d_z0[d_idx] + 0.5*dt * (d_what0[d_idx] + d_what[d_idx])


class MomentumEquationBodyForce(Equation):
    def __init__(self, dest, sources, gx=0.0, gy=0.0, gz=0.0):
        self.gx = gx
        self.gy = gy
        self.gz = gz
        super(MomentumEquationBodyForce, self).__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def post_loop(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] += self.gx
        d_av[d_idx] += self.gy
        d_aw[d_idx] += self.gz


class VelocityDivergence(Equation):
    def initialize(self, d_idx, d_rhs, d_pk, d_p):
        d_rhs[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, s_rho, d_rhs, dt, VIJ, DWIJ):
        Vj = s_m[s_idx] / s_rho[s_idx]
        vdotdwij = VIJ[0]*DWIJ[0] + VIJ[1]*DWIJ[1] + VIJ[2]*DWIJ[2]
        d_rhs[d_idx] += -Vj * vdotdwij / dt


class VelocityDivergenceSolid(Equation):
    def loop(self, d_idx, s_idx, s_m, s_rho, d_rhs, dt, d_u, d_v, d_w,
             s_ug_star, s_vg_star, s_wg_star, DWIJ):
        Vj = s_m[s_idx] / s_rho[s_idx]
        uij = d_u[d_idx] - s_ug_star[s_idx]
        vij = d_v[d_idx] - s_vg_star[s_idx]
        wij = d_w[d_idx] - s_wg_star[s_idx]
        vdotdwij = uij*DWIJ[0] + vij*DWIJ[1] + wij*DWIJ[2]
        d_rhs[d_idx] += -Vj * vdotdwij / dt


class VelocityDivergenceSolidBad(Equation):
    def loop(self, d_idx, s_idx, s_m, s_rho, d_rhs, dt, d_u, d_v, d_w,
             s_ug, s_vg, s_wg, DWIJ):
        Vj = s_m[s_idx] / s_rho[s_idx]
        uij = d_u[d_idx] - s_ug[s_idx]
        vij = d_v[d_idx] - s_vg[s_idx]
        wij = d_w[d_idx] - s_wg[s_idx]
        vdotdwij = uij*DWIJ[0] + vij*DWIJ[1] + wij*DWIJ[2]
        d_rhs[d_idx] += -Vj * vdotdwij / dt


class DensityInvariance(Equation):
    def __init__(self, dest, sources, rho0):
        self.rho0 = rho0
        super(DensityInvariance, self).__init__(dest, sources)

    def post_loop(self, d_idx, d_rho, d_rhs, dt):
        rho0 = self.rho0
        d_rhs[d_idx] = (rho0 - d_rho[d_idx]) / (dt*dt*rho0)


class InitializePk(Equation):
    def initialize(self, d_idx, d_pk, d_p):
        d_pk[d_idx] = d_p[d_idx]


class PressureCoeffMatrixIterative(Equation):
    def initialize(self, d_idx, d_diag, d_odiag_pk):
        d_diag[d_idx] = 0.0
        d_odiag_pk[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho, d_diag, d_odiag_pk, s_pk,
             XIJ, DWIJ, R2IJ, EPS):
        rhoij = (s_rho[s_idx] + d_rho[d_idx])
        rhoij2_1 = 1.0/(d_rho[d_idx]*rhoij)

        xdotdwij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]

        fac = 4.0 * s_m[s_idx] * rhoij2_1 * xdotdwij / (R2IJ + EPS)

        d_diag[d_idx] += fac
        d_odiag_pk[d_idx] += -fac * s_pk[s_idx]


class PressureCoeffMatrixIterativePerf(Equation):
    def initialize(self, d_idx, d_diag, d_nbr_idx, d_no_of_sources, d_offset,
                   d_odiag_coeff, d_odiag_pk, d_ctr):
        d_diag[d_idx] = 0.0
        d_odiag_pk[d_idx] = 0.0
        d_ctr[d_idx] = 0

        j = declare('int')
        for j in range(100):
            d_odiag_coeff[d_idx*100 + j] = 0.0
            d_nbr_idx[d_idx*100 + j] = 0

        for j in range(d_no_of_sources[0]):
            d_offset[d_idx*d_no_of_sources[0] + j] = 0

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho, d_diag, d_odiag_pk, s_pk,
             d_odiag_coeff, d_ctr, d_nbr_idx, d_no_of_sources, s_source_idx,
             d_offset, XIJ, DWIJ, R2IJ, EPS):
        rhoij = (s_rho[s_idx] + d_rho[d_idx])
        rhoij2_1 = 1.0/(d_rho[d_idx]*rhoij)

        xdotdwij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]

        fac = 4.0 * s_m[s_idx] * rhoij2_1 * xdotdwij / (R2IJ + EPS)

        d_diag[d_idx] += fac
        d_odiag_pk[d_idx] += -fac * s_pk[s_idx]

        k, offset_idx = declare('int', 2)
        k = d_ctr[d_idx]
        d_odiag_coeff[d_idx*100 + k] = -fac
        d_nbr_idx[d_idx*100 + k] = s_idx
        d_ctr[d_idx] += 1

        offset_idx = d_idx*d_no_of_sources[0] + s_source_idx[0]
        d_offset[offset_idx] += 1


class PressureCoeffMatrix(Equation):
    def initialize(self, d_idx, d_coeffs, d_nop):
        j = declare('int')
        for j in range(d_nop[0]):
            d_coeffs[d_idx*d_nop[0] + j] = 0

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho, d_coeffs, d_nop, XIJ, DWIJ,
             R2IJ, EPS):
        rhoij = (s_rho[s_idx] + d_rho[d_idx])
        rhoij2_1 = 1.0/(d_rho[d_idx]*rhoij)

        xdotdwij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]

        fac = 4.0 * s_m[s_idx] * rhoij2_1 * xdotdwij / (R2IJ + EPS)

        d_coeffs[d_idx*d_nop[0] + d_idx] += fac
        if s_idx != d_idx:
            d_coeffs[d_idx*d_nop[0] + s_idx] = -fac


class PPESolve(Equation):
    def __init__(self, dest, sources, omega=0.5, tolerance=0.05,
                 max_iterations=1000):
        self.conv = 0.0
        self.omega = omega
        self.tolerance = tolerance
        self.count = 0
        self.max_iterations = max_iterations
        super(PPESolve, self).__init__(dest, sources)

    def initialize(self, d_idx, d_p, d_pk, d_rhs, d_odiag_pk, d_diag, d_pdiff,
                   d_rho, d_m, d_pabs, d_pmax):
        omega = self.omega
        diag = d_diag[d_idx]
        pnew = (d_rhs[d_idx] - d_odiag_pk[d_idx]) / diag
        p = omega * pnew + (1.0 - omega) * d_pk[d_idx]

        d_pdiff[d_idx] = abs(p - d_pk[d_idx])
        d_pabs[d_idx] = abs(p)
        d_p[d_idx] = p
        d_pk[d_idx] = p
        d_pmax[0] = max(abs(d_pmax[0]), d_p[d_idx])

    def reduce(self, dst, t, dt):
        self.count += 1
        dst.iters[0] = self.count
        if dst.gpu is not None:
            from compyle.array import sum
            n = dst.gpu.p.length
            pdiff = sum(dst.gpu.pdiff, dst.backend) / n
            pmean = sum(dst.gpu.pabs, dst.backend) / n
            conv = pdiff/pmean
            if pmean < 1.0:
                conv = pdiff
            self.conv = 1 if conv < self.tolerance else -1
        else:
            pdiff = dst.pdiff.mean()
            pmean = numpy.abs(dst.p).mean()
            conv = pdiff/pmean
            if pmean < 1.0:
                max_rhs = numpy.abs(dst.rhs).max()
                max_diag = numpy.abs(dst.diag).max() / max_rhs
                conv = pdiff*max_diag
            dst.pdiffk[self.count-1] = pdiff
            dst.pdiffk[self.count:] = 0.0
            self.conv = 1 if conv < self.tolerance else -1

    def converged(self):
        if self.conv == 1 and self.count < self.max_iterations:
            self.count = 0
        if self.count >= self.max_iterations:
            self.count = 0
            print("Max iterations exceeded")
        return self.conv


class ComputeOdiagP(Equation):
    def initialize(self, d_idx, d_odiag_pk):
        d_odiag_pk[d_idx] = 0.0

    def loop_all(self, d_idx, d_odiag_coeff, d_nbr_idx, d_odiag_pk, s_pk, NBRS,
                 N_NBRS, d_offset, s_source_idx, d_no_of_sources):
        j, idx, offset, nbr_idx = declare('int', 4)

        if s_source_idx[0] == 0:
            offset = 0
        else:
            offset = d_offset[d_idx*d_no_of_sources[0] + s_source_idx[0] - 1]
        for j in range(N_NBRS):
            idx = d_idx*100 + offset + j
            nbr_idx = d_nbr_idx[idx]
            d_odiag_pk[d_idx] += d_odiag_coeff[idx] * (s_pk[nbr_idx])


class UpdateGhostPressure(Equation):
    def initialize(self, d_idx, d_tag, d_gid, d_p, d_pk):
        idx = declare('int')
        if d_tag[d_idx] == 2:
            idx = d_gid[d_idx]
            d_pk[d_idx] = d_pk[idx]
            d_p[d_idx] = d_p[idx]
            # d_residue[d_idx] = d_residue[idx]
            # d_conj[d_idx] = d_conj[idx]


class UpdateGhostDensity(Equation):
    def initialize(self, d_idx, d_tag, d_gid, d_rho):
        idx = declare('int')
        if d_tag[d_idx] == 2:
            idx = d_gid[d_idx]
            d_rho[d_idx] = d_rho[idx]


class MomentumEquationPressureGradient(Equation):
    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_p, s_p, d_rho, s_rho, d_au,
             d_av, d_aw, DWIJ):
        Vj = s_m[s_idx] / s_rho[s_idx]
        pji = (s_p[s_idx] - d_p[d_idx])
        fac = -Vj * pji / d_rho[d_idx]

        d_au[d_idx] += fac * DWIJ[0]
        d_av[d_idx] += fac * DWIJ[1]
        d_aw[d_idx] += fac * DWIJ[2]


class MomentumEquationPressureGradientSymmetric(Equation):
    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_p, s_p, d_rho, s_rho, d_au, d_av, d_aw,
             DWIJ):
        rhoi2 = d_rho[d_idx]*d_rho[d_idx]
        rhoj2 = s_rho[s_idx]*s_rho[s_idx]
        pij = d_p[d_idx]/rhoi2 + s_p[s_idx]/rhoj2
        fac = -s_m[s_idx] * pij

        d_au[d_idx] += fac * DWIJ[0]
        d_av[d_idx] += fac * DWIJ[1]
        d_aw[d_idx] += fac * DWIJ[2]


class EvaluateNumberDensity(Equation):
    def initialize(self, d_idx, d_wij, d_wij2):
        d_wij[d_idx] = 0.0
        d_wij2[d_idx] = 0.0

    def loop(self, d_idx, d_wij, d_wij2, XIJ, HIJ, RIJ, SPH_KERNEL):
        wij = SPH_KERNEL.kernel(XIJ, RIJ, HIJ)
        wij2 = SPH_KERNEL.kernel(XIJ, RIJ, 0.5*HIJ)
        d_wij[d_idx] += wij
        d_wij2[d_idx] += wij2


class SetPressureSolid(Equation):
    def __init__(self, dest, sources, gx=0.0, gy=0.0, gz=0.0,
                 hg_correction=True):
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.hg_correction = hg_correction
        super(SetPressureSolid, self).__init__(dest, sources)

    def initialize(self, d_idx, d_p):
        d_p[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_p, s_p, s_rho,
             d_au, d_av, d_aw, XIJ, RIJ, HIJ, d_wij2, SPH_KERNEL):

        # numerator of Eq. (27) ax, ay and az are the prescribed wall
        # accelerations which must be defined for the wall boundary
        # particle
        wij = SPH_KERNEL.kernel(XIJ, RIJ, HIJ)
        wij2 = SPH_KERNEL.kernel(XIJ, RIJ, 0.5*HIJ)

        gdotxij = (self.gx - d_au[d_idx])*XIJ[0] + \
            (self.gy - d_av[d_idx])*XIJ[1] + \
            (self.gz - d_aw[d_idx])*XIJ[2]

        if d_wij2[d_idx] > 1e-14:
            d_p[d_idx] += s_p[s_idx]*wij2 + s_rho[s_idx]*gdotxij*wij2
        else:
            d_p[d_idx] += s_p[s_idx]*wij + s_rho[s_idx]*gdotxij*wij

    def post_loop(self, d_idx, d_wij, d_wij2, d_p, d_rho, d_pk):
        # extrapolated pressure at the ghost particle
        if d_wij2[d_idx] > 1e-14:
            d_p[d_idx] /= d_wij2[d_idx]
        elif d_wij[d_idx] > 1e-14:
            d_p[d_idx] /= d_wij[d_idx]
        if self.hg_correction:
            d_p[d_idx] = max(0.0, d_p[d_idx])
        d_pk[d_idx] = d_p[d_idx]


class GTVFAcceleration(Equation):
    def __init__(self, dest, sources, pref, internal_flow=False,
                 use_pref=False):
        self.pref = pref
        assert self.pref is not None, "pref should not be None"
        self.internal = internal_flow
        self.hij_fac = 1 if self.internal else 0.5
        self.use_pref = use_pref
        super(GTVFAcceleration, self).__init__(dest, sources)

    def initialize(self, d_idx, d_auhat, d_avhat, d_awhat, d_p0, d_p, d_pmax):
        d_auhat[d_idx] = 0.0
        d_avhat[d_idx] = 0.0
        d_awhat[d_idx] = 0.0

        if self.internal:
            if self.use_pref:
                d_p0[d_idx] = self.pref
            else:
                pref = 2*d_pmax[0]
                d_p0[d_idx] = pref
        else:
            d_p0[d_idx] = min(10*abs(d_p[d_idx]), self.pref)

    def loop(self, d_p0, s_m, s_idx, d_rho, d_idx, d_auhat, d_avhat,
             d_awhat, XIJ, RIJ, SPH_KERNEL,
             HIJ):
        rhoi2 = d_rho[d_idx]*d_rho[d_idx]
        tmp = -d_p0[d_idx] * s_m[s_idx]/rhoi2

        dwijhat = declare('matrix(3)')
        SPH_KERNEL.gradient(XIJ, RIJ, self.hij_fac*HIJ, dwijhat)

        d_auhat[d_idx] += tmp * dwijhat[0]
        d_avhat[d_idx] += tmp * dwijhat[1]
        d_awhat[d_idx] += tmp * dwijhat[2]


#class GTVFAccelerationInternal(Equation):
class GTVFAcceleration1(Equation):
    def __init__(self, dest, sources, pref, hij_fac=0.5):
        self.hij_fac = hij_fac
        self.pref = pref
        assert self.pref is not None, "pref should not be None"
        super(GTVFAcceleration1, self).__init__(dest, sources)

    def initialize(self, d_idx, d_auhat, d_avhat, d_awhat, d_p0, d_p):
        d_auhat[d_idx] = 0.0
        d_avhat[d_idx] = 0.0
        d_awhat[d_idx] = 0.0

        d_p0[d_idx] = self.pref  #min(10*abs(d_p[d_idx]), self.pref)

    def loop(self, d_p0, s_m, s_idx, d_rho, d_idx, d_auhat, d_avhat,
             d_awhat, XIJ, RIJ, SPH_KERNEL,
             HIJ, d_h, dt):
        rhoi2 = d_rho[d_idx]*d_rho[d_idx]
        tmp = -d_p0[d_idx] * s_m[s_idx]/rhoi2

        dwijhat = declare('matrix(3)')
        SPH_KERNEL.gradient(XIJ, RIJ, self.hij_fac*HIJ, dwijhat)

        d_auhat[d_idx] += tmp * dwijhat[0]
        d_avhat[d_idx] += tmp * dwijhat[1]
        d_awhat[d_idx] += tmp * dwijhat[2]


class SmoothedVelocity(Equation):
    def initialize(self, d_ax, d_ay, d_az, d_idx):
        d_ax[d_idx] = 0.0
        d_ay[d_idx] = 0.0
        d_az[d_idx] = 0.0

    def loop(self, d_ax, d_ay, d_az, d_idx, s_uhat, s_vhat, s_what, s_idx, s_m,
             s_rho, WIJ):
        fac = s_m[s_idx]*WIJ / s_rho[s_idx]
        d_ax[d_idx] += fac*s_uhat[s_idx]
        d_ay[d_idx] += fac*s_vhat[s_idx]
        d_az[d_idx] += fac*s_what[s_idx]


class SolidWallNoSlipBC(Equation):
    def __init__(self, dest, sources, nu):
        self.nu = nu
        super(SolidWallNoSlipBC, self).__init__(dest, sources)

    def loop(self, d_idx, s_idx, d_m, d_rho, s_rho, s_m,
             d_u, d_v, d_w,
             d_au, d_av, d_aw,
             s_ug, s_vg, s_wg,
             DWIJ, R2IJ, EPS, XIJ):
        mj = s_m[s_idx]
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]
        rhoij1 = 1.0/(rhoi + rhoj)

        Fij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]

        tmp = mj * 4 * self.nu * rhoij1 * Fij/(R2IJ + EPS)

        d_au[d_idx] += tmp * (d_u[d_idx] - s_ug[s_idx])
        d_av[d_idx] += tmp * (d_v[d_idx] - s_vg[s_idx])
        d_aw[d_idx] += tmp * (d_w[d_idx] - s_wg[s_idx])


class SummationDensity(Equation):
    def initialize(self, d_idx, d_rho):
        d_rho[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, s_m, WIJ):
        d_rho[d_idx] += s_m[s_idx]*WIJ


class SlipVelocityExtrapolation(Equation):
    '''Slip boundary condition on the wall

    The velocity of the fluid is extrapolated over to the wall using
    shepard extrapolation. The velocity normal to the wall is reflected back
    to impose no penetration.
    '''
    def initialize(self, d_idx, d_ug_star, d_vg_star, d_wg_star):
        d_ug_star[d_idx] = 0.0
        d_vg_star[d_idx] = 0.0
        d_wg_star[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_ug_star, d_vg_star, d_wg_star, d_wij, s_u,
             s_v, s_w, d_wij2, XIJ, RIJ, HIJ, SPH_KERNEL):
        wij = SPH_KERNEL.kernel(XIJ, RIJ, HIJ)
        wij2 = SPH_KERNEL.kernel(XIJ, RIJ, 0.5*HIJ)

        if d_wij2[d_idx] > 1e-14:
            d_ug_star[d_idx] += s_u[s_idx]*wij2
            d_vg_star[d_idx] += s_v[s_idx]*wij2
            d_wg_star[d_idx] += s_w[s_idx]*wij2
        else:
            d_ug_star[d_idx] += s_u[s_idx]*wij
            d_vg_star[d_idx] += s_v[s_idx]*wij
            d_wg_star[d_idx] += s_w[s_idx]*wij

    def post_loop(self, d_idx, d_wij, d_wij2, d_ug_star, d_vg_star,
                  d_wg_star, d_normal, d_u, d_v, d_w):
        idx = declare('int')
        idx = 3*d_idx
        if d_wij2[d_idx] > 1e-14:
            d_ug_star[d_idx] /= d_wij2[d_idx]
            d_vg_star[d_idx] /= d_wij2[d_idx]
            d_wg_star[d_idx] /= d_wij2[d_idx]
        elif d_wij[d_idx] > 1e-14:
            d_ug_star[d_idx] /= d_wij[d_idx]
            d_vg_star[d_idx] /= d_wij[d_idx]
            d_wg_star[d_idx] /= d_wij[d_idx]

        # u_g \cdot n = 2*(u_wall \cdot n ) - (u_f \cdot n)
        # u_g \cdot t = (u_f \cdot t) = u_f - (u_f \cdot n)
        tmp1 = d_u[d_idx] - d_ug_star[d_idx]
        tmp2 = d_v[d_idx] - d_vg_star[d_idx]
        tmp3 = d_w[d_idx] - d_wg_star[d_idx]

        projection = (tmp1*d_normal[idx] +
                      tmp2*d_normal[idx+1] +
                      tmp3*d_normal[idx+2])

        d_ug_star[d_idx] += 2*projection * d_normal[idx]
        d_vg_star[d_idx] += 2*projection * d_normal[idx+1]
        d_wg_star[d_idx] += 2*projection * d_normal[idx+2]


class InitializeUhat(Equation):
    def initialize(self, d_idx, d_dummy_uhat, d_dummy_vhat, d_dummy_what,
                   d_dummy_x, d_dummy_y, d_dummy_z,
                   d_x, d_y, d_z):
        d_dummy_uhat[d_idx] = 0.0
        d_dummy_vhat[d_idx] = 0.0
        d_dummy_what[d_idx] = 0.0

        d_dummy_x[d_idx] = 0.0
        d_dummy_y[d_idx] = 0.0
        d_dummy_z[d_idx] = 0.0


class GTVFUpdatePosition(Equation):
    def __init__(self, dest, sources, dt_fac):
        self.dt_fac = dt_fac
        super(GTVFUpdatePosition, self).__init__(dest, sources)

    def post_loop(self, d_idx, d_x, d_y, d_z, d_auhat, d_avhat, d_awhat,
                  d_dummy_uhat, d_dummy_vhat, d_dummy_what, dt,
                  d_dummy_x, d_dummy_y, d_dummy_z):
        dt = dt/self.dt_fac
        dt2b2 = 0.5*dt*dt

        d_x[d_idx] += dt*d_dummy_uhat[d_idx] + dt2b2*d_auhat[d_idx]
        d_y[d_idx] += dt*d_dummy_vhat[d_idx] + dt2b2*d_avhat[d_idx]
        d_z[d_idx] += dt*d_dummy_what[d_idx] + dt2b2*d_awhat[d_idx]

        d_dummy_x[d_idx] += dt*d_dummy_uhat[d_idx] + dt2b2*d_auhat[d_idx]
        d_dummy_y[d_idx] += dt*d_dummy_vhat[d_idx] + dt2b2*d_avhat[d_idx]
        d_dummy_z[d_idx] += dt*d_dummy_what[d_idx] + dt2b2*d_awhat[d_idx]

        d_dummy_uhat[d_idx] += dt*d_auhat[d_idx]
        d_dummy_vhat[d_idx] += dt*d_avhat[d_idx]
        d_dummy_what[d_idx] += dt*d_awhat[d_idx]


def gtvf_correction(dim, fluids, sources, pref, internal_flow,
                    use_pref, iters):
    eqns = []
    for fluid in fluids:
        eqns.append(Group(
            equations=[InitializeUhat(dest=fluid, sources=None)]
        ))
    for fluid in fluids:
        eqns.append(Group(equations=[
            GTVFAcceleration(
                dest=fluid, sources=sources, pref=pref,
                internal_flow=internal_flow, use_pref=use_pref
            ),
            GTVFUpdatePosition(dest=fluid, sources=None, dt_fac=float(iters))
        ], iterate=True, min_iterations=iters, max_iterations=iters))
    return eqns


class EvaluateKappa(Equation):
    '''
    An improved free surface modeling for incompressible SPH
    Prapanch Nair, Gaurav Tomar
    '''
    def initialize(self, d_idx, d_kappa, t, dt):
        if t < 1.5*dt:
            d_kappa[d_idx] = 0.0

    def loop(self, d_idx, d_rho, s_rho, s_idx, s_m, d_kappa, DWIJ, XIJ, R2IJ,
             EPS, t, dt):
        if t < 1.5*dt:
            Vj = s_m[s_idx] / s_rho[s_idx]
            rhoinv = 1.0/(d_rho[d_idx] + s_rho[s_idx])
            xdotdwij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]
            d_kappa[d_idx] += 4.0 * rhoinv * xdotdwij * Vj / (R2IJ + EPS)


class FreeSurfaceCorrection(Equation):
    """An improved free surface modeling for incompressible SPH, Prapanch Nair,
    Gaurav Tomar

    This should in fact be multiplied by the pressure of the surrounding
    particle but for a free surface the pressure of the surrounding particle is
    set to zero.

    """
    def __init__(self, dest, sources, rho0, free_surf_cutoff=0.5):
        self.rho0 = rho0
        self.free_surf_cutoff = free_surf_cutoff
        super(FreeSurfaceCorrection, self).__init__(dest, sources)

    def py_initialize(self, dst, t, dt):
        if t < 1.5*dt:
            idx = numpy.argmax(dst.rho)
            dst.kmax[0] = -dst.kappa[idx]

    def post_loop(self, d_idx, d_rho, d_diag, d_kmax, d_pk, d_rhs, d_odiag_pk,
                  d_odiag_coeff):
        if d_rho[d_idx]/self.rho0 < self.free_surf_cutoff:
            d_diag[d_idx] = d_kmax[0]


class FreeSurfaceBC(Equation):
    def __init__(self, dest, sources, rho0, rho_cutoff=0.8):
        self.rho0 = rho0
        self.rho_cutoff = rho_cutoff
        super(FreeSurfaceBC, self).__init__(dest, sources)

    def post_loop(self, d_idx, d_diag, d_rho, d_odiag_pk, d_rhs,
                  d_odiag_coeff, d_pk):
        j = declare('int')
        rho = d_rho[d_idx] / self.rho0
        # The diag[d_idx] < 1e-12 condition is not needed, since if the particle
        # density is less than rho_cutoff then its diag[d_idx] goes to zero.
        if abs(d_diag[d_idx]) < 1e-12 or rho < self.rho_cutoff:
            d_diag[d_idx] = 1.0
            # Divergence on the free surface particles should be zero!
            d_rhs[d_idx] = 0.0
            d_odiag_pk[d_idx] = 0.0
            # Setting current pressure to zero is needed as we use the old value
            # in the update of Jacobi iteration.
            d_pk[d_idx] = 0.0

            for j in range(100):
                d_odiag_coeff[d_idx*100 + j] = 0.0


class FreeSurfaceBCMatrix(Equation):
    def __init__(self, dest, sources, rho0, rho_cutoff=0.8):
        self.rho0 = rho0
        self.rho_cutoff = rho_cutoff
        super(FreeSurfaceBCMatrix, self).__init__(dest, sources)

    def post_loop(self, d_idx, d_rhs, d_rho, d_coeffs, d_nop):
        j = declare('int')
        diag = d_coeffs[d_idx*d_nop[0] + d_idx]
        rho = d_rho[d_idx] / self.rho0
        if abs(diag) < 1e-12 or rho < self.rho_cutoff:
            d_rhs[d_idx] = 0.0
            for j in range(d_nop[0]):
                d_coeffs[d_idx*d_nop[0] + j] = 0.0
            d_coeffs[d_idx*d_nop[0] + d_idx] = 1.0


class SolveMatrix(Equation):
    def __init__(self, dest, sources, tol):
        self.tol = tol
        super(SolveMatrix, self).__init__(dest, sources)

    def py_initialize(self, dst, t, dt):
        A = declare('object')
        a = declare('object')
        A = dst.coeffs.reshape((dst.nop[0], dst.nop[0]))
        a = scipy.sparse.csr_matrix(A)

        sol, ec = bicgstab(a, dst.rhs, x0=dst.pk, tol=self.tol)
        dst.p[:] = sol
        assert ec == 0


class SISPHScheme(Scheme):
    def __init__(self, fluids, solids, dim, nu, rho0, c0, alpha=0.0, beta=0.0,
                 gx=0.0, gy=0.0, gz=0.0, tolerance=0.05,
                 omega=0.5, hg_correction=False, has_ghosts=False,
                 inviscid_solids=None, inlet_outlet_manager=None, pref=None,
                 gtvf=False, symmetric=False, rho_cutoff=0.8,
                 max_iterations=1000, internal_flow=False,
                 use_pref=False, bad_bc=False, domain=None, gtvf_steps=5,
                 free_surf=False, bicgstab=False, cache_matrix=False,
                 matrix=False):
        self.fluids = fluids
        self.solids = solids
        self.solver = None
        self.dim = dim
        self.nu = nu
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.c0 = c0
        self.alpha = alpha
        self.beta = beta
        self.rho0 = rho0
        self.rho_cutoff = rho_cutoff
        self.tolerance = tolerance
        self.omega = omega
        self.hg_correction = hg_correction
        self.has_ghosts = has_ghosts
        self.inviscid_solids = [] if inviscid_solids is None else\
            inviscid_solids
        self.inlet_outlet_manager = None if inlet_outlet_manager is None\
            else inlet_outlet_manager
        self.fluid_with_io = self.fluids.copy()
        self.pref = pref
        self.gtvf = gtvf
        self.symmetric = symmetric
        self.max_iterations = max_iterations
        self.internal_flow = internal_flow
        self.use_pref = use_pref
        self.bad_bc = bad_bc
        self.domain = domain
        self.gtvf_steps = gtvf_steps
        self.free_surf = free_surf
        self.bicgstab = bicgstab
        self.cache_matrix = cache_matrix
        self.matrix = matrix

    def add_user_options(self, group):
        group.add_argument(
            "--tol", action="store", dest="tolerance",
            type=float,
            help="Tolerance for convergence."
        )
        group.add_argument(
            "--omega", action="store", dest="omega",
            type=float,
            help="Omega for convergence."
        )
        group.add_argument(
            '--alpha', action='store', type=float, dest='alpha',
            default=None,
            help='Artificial viscosity.'
        )
        group.add_argument(
            "--max-iters", action="store", dest="max_iterations",
            type=int,
            help="Maximum no of iterations to be used in the PPE."
        )
        add_bool_argument(
            group, 'gtvf', dest='gtvf', default=None,
            help='Use GTVF.'
        )
        add_bool_argument(
            group, 'symmetric', dest='symmetric', default=None,
            help='Use symmetric form of pressure gradient.'
        )
        add_bool_argument(
            group, 'internal', dest='internal_flow', default=None,
            help='If the simulation is internal or external.'
        )
        add_bool_argument(
            group, 'bad-bc', dest='bad_bc', default=None,
            help='Use the no slip u*.'
        )
        group.add_argument(
            '--gtvf-steps', action='store', dest='gtvf_steps',
            default=None, type=int, help='No. of steps to GTVF.'
        )
        add_bool_argument(
            group, 'free-surf', dest='free_surf', default=None,
            help='Use free surface boundary correction.'
        )
        add_bool_argument(
            group, 'bicgstab', dest='bicgstab', default=None,
            help='Use BiCGStab to solve the PPE.'
        )
        add_bool_argument(
            group, 'perf', dest='cache_matrix', default=None,
            help='Cache the coefficient matrix.'
        )
        add_bool_argument(
            group, 'matrix', dest='matrix', default=None,
            help='Use scipy bicgstab to solve.'
        )

    def consume_user_options(self, options):
        _vars = [
            'tolerance', 'omega', 'alpha', 'gtvf', 'symmetric', 'internal_flow',
            'max_iterations', 'bad_bc', 'gtvf_steps', 'free_surf', 'bicgstab',
            'cache_matrix', 'matrix'
        ]
        data = dict((var, self._smart_getattr(options, var))
                    for var in _vars)
        self.configure(**data)

    def configure_solver(self, kernel=None, integrator_cls=None,
                         extra_steppers=None, **kw):
        import pysph.base.kernels as kern
        if kernel is None:
            kernel = kern.QuinticSpline(dim=self.dim)
        steppers = {}
        if extra_steppers is not None:
            steppers.update(extra_steppers)

        if self.gtvf:
            step_cls = SISPHGTVFStep
        else:
            step_cls = SISPHStep

        for fluid in self.fluids:
            if fluid not in steppers:
                steppers[fluid] = step_cls()

        if integrator_cls is not None:
            cls = integrator_cls
        else:
            if self.gtvf:
                cls = PECIntegratorGTVF
            else:
                cls = PECIntegrator

        iom = self.inlet_outlet_manager
        if iom is not None:
            iom_stepper = iom.get_stepper(self, cls)
            for name in iom_stepper:
                steppers[name] = iom_stepper[name]

        integrator = cls(**steppers)

        from pysph.solver.solver import Solver
        self.solver = Solver(
            dim=self.dim, integrator=integrator, kernel=kernel, **kw
        )

        if iom is not None:
            iom.setup_iom(dim=self.dim, kernel=kernel)

    def _get_velocity_bc(self, ustar=False):
        from wall_normal import SetWallVelocityNew as SetWallVelocity

        grps = []

        eqs = []
        all_solids = self.solids + self.inviscid_solids
        for solid in all_solids:
            eqs.append(
                EvaluateNumberDensity(
                    dest=solid, sources=self.fluid_with_io
                )
            )

        grps.append(Group(equations=eqs))

        if not ustar:
            eqs = [SetWallVelocity(dest=s, sources=self.fluid_with_io)
                   for s in self.solids]
        else:
            if self.bad_bc:
                eqs = [SetWallVelocity(dest=s, sources=self.fluid_with_io)
                       for s in self.solids]
            else:
                eqs = [
                    SlipVelocityExtrapolation(
                        dest=s, sources=self.fluid_with_io
                    ) for s in self.solids
                ]

        for solids in self.inviscid_solids:
            eqs.append(
                SlipVelocityExtrapolation(
                    dest=solids, sources=self.fluid_with_io
                )
            )

        grps.append(Group(equations=eqs))
        return grps

    def _get_pressure_bc(self):
        grps = []
        eqs = []
        all_solids = self.solids + self.inviscid_solids
        for solid in all_solids:
            eqs.append(
                EvaluateNumberDensity(
                    dest=solid, sources=self.fluid_with_io
                )
            )
        grps.append(Group(equations=eqs))

        eqs = []
        for solid in all_solids:
            eqs.append(
                SetPressureSolid(
                    dest=solid, sources=self.fluid_with_io,
                    gx=self.gx, gy=self.gy, gz=self.gz,
                    hg_correction=self.hg_correction
                )
            )
        grps.append(Group(equations=eqs))

        return grps

    def _get_normals(self, pa):
        from pysph.tools.sph_evaluator import SPHEvaluator
        from wall_normal import ComputeNormals, SmoothNormals

        pa.add_property('normal', stride=3)
        pa.add_property('normal_tmp', stride=3)

        name = pa.name

        props = ['m', 'rho', 'h']
        for p in props:
            x = pa.get(p)
            if numpy.all(x < 1e-12):
                msg = f'WARNING: cannot compute normals "{p}" is zero'
                print(msg)

        seval = SPHEvaluator(
            arrays=[pa], equations=[
                Group(equations=[
                    ComputeNormals(dest=name, sources=[name])
                ]),
                Group(equations=[
                    SmoothNormals(dest=name, sources=[name])
                ]),
            ],
            dim=self.dim, domain_manager=self.domain
        )
        seval.evaluate()

    def _get_viscous_eqns(self):
        from pysph.sph.wc.transport_velocity import (
            MomentumEquationArtificialViscosity)
        from pysph.sph.wc.viscosity import LaminarViscosity
        from pysph.sph.wc.gtvf import MomentumEquationArtificialStress

        iom = self.inlet_outlet_manager
        if iom is not None:
            self.fluid_with_io = self.fluids + iom.get_io_names()
        all = self.fluid_with_io + self.solids + self.inviscid_solids

        eq, stg = [], []
        for fluid in self.fluids:
            eq.append(SummationDensity(dest=fluid, sources=all))
        stg.append(Group(equations=eq))

        if self.has_ghosts:
            eq = []
            for fluid in self.fluids:
                eq.append(UpdateGhostDensity(dest=fluid, sources=all))
            stg.append(Group(equations=eq, real=False))

        eq = []
        for fluid in self.fluids:
            if self.nu > 0.0:
                eq.append(
                    LaminarViscosity(fluid, sources=self.fluids,
                                     nu=self.nu)
                )
            if self.alpha > 0.0:
                eq.append(
                    MomentumEquationArtificialViscosity(
                        dest=fluid, sources=self.fluids, c0=self.c0,
                        alpha=self.alpha
                    )
                )
            eq.append(
                MomentumEquationBodyForce(
                    fluid, sources=None, gx=self.gx, gy=self.gy,
                    gz=self.gz)
            )
            if self.gtvf:
                eq.append(
                    MomentumEquationArtificialStress(
                        dest=fluid, sources=self.fluids, dim=self.dim)
                )
            if self.solids and self.nu > 0.0:
                eq.append(
                    SolidWallNoSlipBC(
                        dest=fluid, sources=self.solids, nu=self.nu
                    )
                )
        stg.append(Group(equations=eq))
        return stg

    def _get_ppe(self):
        from pysph.sph.wc.transport_velocity import VolumeSummation

        iom = self.inlet_outlet_manager
        if iom is not None:
            self.fluid_with_io = self.fluids + iom.get_io_names()
        all = self.fluid_with_io + self.solids + self.inviscid_solids
        all_solids = self.solids + self.inviscid_solids

        if self.bad_bc:
            DivergenceSolid = VelocityDivergenceSolidBad
        else:
            DivergenceSolid = VelocityDivergenceSolid

        eq, stg = [], []

        # Not needed.
        for fluid in self.fluid_with_io:
            eq.append(SummationDensity(dest=fluid, sources=all))
        stg.append(Group(equations=eq))

        if self.has_ghosts:
            eq = []
            for fluid in self.fluids:
                eq.append(UpdateGhostDensity(dest=fluid, sources=all))
            stg.append(Group(equations=eq, real=False))

        eq2 = []
        for fluid in self.fluid_with_io:
            # Needed for fpc?
            eq2.append(InitializePk(dest=fluid, sources=None))
            eq2.append(VolumeSummation(dest=fluid, sources=all))
            eq2.append(
                VelocityDivergence(
                    dest=fluid,
                    sources=self.fluid_with_io+self.inviscid_solids
                )
            )

            if self.solids:
                eq2.append(
                    DivergenceSolid(fluid, sources=self.solids)
                )

            if self.free_surf:
                eq2.append(EvaluateKappa(dest=fluid, sources=all))

            if self.cache_matrix and not self.bicgstab:
                eq2.append(
                    PressureCoeffMatrixIterativePerf(
                        fluid, sources=all
                    )
                )
        stg.append(Group(equations=eq2))

        if self.bicgstab:
            from bicgstab import bicgstab_equations

            stg.extend(
                bicgstab_equations(
                    self.fluids, self.solids, self.inviscid_solids, iom,
                    self.rho0, self.rho_cutoff, self.tolerance,
                    self.max_iterations, gx=self.gx, gy=self.gy, gz=self.gz,
                    hg_correction=self.hg_correction, perf=self.cache_matrix
                )
            )
        elif self.matrix:
            stg.extend(self._get_matrix_eqns())
        else:
            eqns = self._get_jacobi_equations()
            stg.extend(eqns)

        if self.has_ghosts:
            ghost_eqns = Group(
                equations=[UpdateGhostPressure(dest=fluid, sources=None)
                           for fluid in self.fluid_with_io],
                real=False
            )
            stg.append(ghost_eqns)
        return stg

    def _get_matrix_eqns(self):
        iom = self.inlet_outlet_manager
        if iom is not None:
            self.fluid_with_io = self.fluids + iom.get_io_names()
        all = self.fluid_with_io + self.solids + self.inviscid_solids
        all_solids = self.solids + self.inviscid_solids

        fluids_with_input = self.fluid_with_io.copy()
        if 'outlet' in self.fluid_with_io:
            fluids_with_input.remove('outlet')


        solver_eqns = []
        eq = []
        for fluid in fluids_with_input:
            eq.append(PressureCoeffMatrix(fluid, sources=all))
        solver_eqns.append(Group(equations=eq))

        eq = []
        for fluid in fluids_with_input:
            eq.append(
                FreeSurfaceBCMatrix(
                    dest=fluid, sources=None, rho0=self.rho0,
                    rho_cutoff=self.rho_cutoff
                )
            )
        solver_eqns.append(Group(equations=eq))

        eq = []
        for fluid in fluids_with_input:
            eq.append(SolveMatrix(dest=fluid, sources=None, tol=self.tolerance))
            solver_eqns.append(Group(equations=eq))
        return solver_eqns

    def _get_jacobi_equations(self):
        iom = self.inlet_outlet_manager
        if iom is not None:
            self.fluid_with_io = self.fluids + iom.get_io_names()
        all = self.fluid_with_io + self.solids + self.inviscid_solids
        all_solids = self.solids + self.inviscid_solids

        fluid_with_input = self.fluid_with_io.copy()
        if 'outlet' in self.fluid_with_io:
            fluid_with_input.remove('outlet')

        solver_eqns = []
        if self.has_ghosts:
            ghost_eqns = Group(
                equations=[UpdateGhostPressure(dest=fluid, sources=None)
                           for fluid in self.fluid_with_io],
                real=False
            )
            solver_eqns = [ghost_eqns]

        if all_solids:
            solver_eqns.extend(self._get_pressure_bc())

        if not self.cache_matrix:
            eqns = []
            for fluid in fluid_with_input:
                eqns.append(
                    PressureCoeffMatrixIterative(fluid, sources=all)
                )
            solver_eqns.append(Group(equations=eqns))

        eqns = []
        for fluid in fluid_with_input:
            if self.cache_matrix:
                eqns.append(
                    ComputeOdiagP(dest=fluid, sources=all)
                )
            if self.free_surf:
                eqns.append(
                    FreeSurfaceCorrection(
                        dest=fluid, sources=None, rho0=self.rho0,
                        free_surf_cutoff=0.95
                    )
                )
            eqns.append(
                FreeSurfaceBC(
                    dest=fluid, sources=None, rho0=self.rho0,
                    rho_cutoff=self.rho_cutoff
                )
            )
        solver_eqns.append(Group(equations=eqns))

        eqns = []
        for fluid in fluid_with_input:
            eqns.append(
                PPESolve(
                    dest=fluid, sources=all, tolerance=self.tolerance,
                    omega=self.omega, max_iterations=self.max_iterations,
                )
            )
        solver_eqns.append(Group(equations=eqns))

        stg = [
            Group(
                equations=solver_eqns, iterate=True,
                max_iterations=self.max_iterations,
                min_iterations=min(self.max_iterations, 2)
            )
        ]
        return stg

    def get_equations(self):
        iom = self.inlet_outlet_manager
        if iom is not None:
            self.fluid_with_io = self.fluids + iom.get_io_names()
        all = self.fluid_with_io + self.solids + self.inviscid_solids
        all_solids = self.solids + self.inviscid_solids

        stg1 = []
        if all_solids:
            g0 = self._get_velocity_bc()
            stg1.extend(g0)

        if iom is not None:
            io_eqs = iom.get_equations(self)
            for grp in io_eqs:
                stg1.append(grp)

        stg1.extend(self._get_viscous_eqns())

        stg2 = []

        if all_solids:
            g0 = self._get_velocity_bc(ustar=True)
            stg2.extend(g0)

        stg2.extend(self._get_ppe())

        if iom is not None:
            io_eqs = iom.get_equations(self)
            stg2.extend(io_eqs)

        if all_solids:
            stg2.extend(self._get_pressure_bc())
            stg2.extend(self._get_velocity_bc())

        eq4 = []
        for fluid in self.fluids:
            if self.symmetric:
                eq4.append(
                    MomentumEquationPressureGradientSymmetric(fluid, all)
                )
            else:
                eq4.append(
                    MomentumEquationPressureGradient(fluid, sources=all)
                )
        stg2.append(Group(equations=eq4))
        ms_eqs = [stg1, stg2]

        if self.gtvf:
            stg3 = []

            if iom is not None:
                io_eqs = iom.get_equations(self)
                stg3.extend(io_eqs)

            eqns = gtvf_correction(
                self.dim, self.fluids, all, pref=self.pref,
                internal_flow=self.internal_flow,
                use_pref=self.use_pref, iters=self.gtvf_steps
            )
            stg3.extend(eqns)
            ms_eqs.append(stg3)
        all_eqns = MultiStageEquations(ms_eqs)
        return all_eqns

    def setup_properties(self, particles, clean=True):
        all_arrays = self.fluid_with_io + self.solids + self.inviscid_solids

        particle_arrays = dict([(p.name, p) for p in particles])
        dummy = get_particle_array_sisph(name='junk',
                                         gid=particle_arrays['fluid'].gid)
        props = []
        for name, array in dummy.properties.items():
            d = dict(name=name, type=array.get_c_type())
            if name in dummy.stride:
                d.update({'stride': dummy.stride[name]})
            props.append(d)

        constants = []
        for name, array in dummy.constants.items():
            d = dict(name=name, data=array)
            constants.append(d)

        output_props = dummy.output_property_arrays
        iom = self.inlet_outlet_manager

        for fluid in self.fluid_with_io:
            pa = particle_arrays[fluid]
            self._ensure_properties(pa, props, clean)
            pa.set_output_arrays(output_props)
            for const in constants:
                pa.add_constant(**const)
            if iom is not None:
                iom.add_io_properties(pa, self)

        solid_props = ['wij', 'wij2', 'ug', 'vg', 'wg', 'uf', 'vf',
                       'wf', 'pk', 'V', 'ug_star', 'vg_star',
                       'wg_star', 'Conj', 'S', 'Conj_hat', 'S_hat']
        all_solids = self.solids + self.inviscid_solids
        for solid in all_solids:
            pa = particle_arrays[solid]
            for prop in solid_props:
                pa.add_property(prop)
            self._get_normals(pa)
            pa.add_output_arrays(['p', 'ug', 'vg', 'wg', 'normal'])
            pa.add_output_arrays(['ug_star', 'vg_star', 'wg_star'])

        if self.cache_matrix:
            for fluid in self.fluid_with_io:
                pa = particle_arrays[fluid]
                pa.add_constant('no_of_sources', [0])
                pa.no_of_sources[0] = len(all_arrays)
                pa.add_property('offset', stride=len(all_arrays), type='int')

            for i, arrays in enumerate(all_arrays):
                pa = particle_arrays[arrays]
                pa.add_constant('source_idx', [0])
                pa.source_idx[0] = i

        if self.matrix:
            for fluid in self.fluid_with_io:
                pa.add_constant('nop', [len(pa.x)])
                pa.add_property('coeffs', stride=len(pa.x))
