"""Dam break solved with DTSPH.
"""
import numpy as np

from pysph.examples import dam_break_2d as DB
from pysph.tools.geometry import get_2d_tank, get_2d_block
from pysph.base.utils import get_particle_array
from sisph import SISPHScheme

fluid_column_height = 2.0
fluid_column_width = 1.0
container_height = 4.0
container_width = 4
nboundary_layers = 4

g = 9.81
h = 0.0390
ro = 1000.0
vref = np.sqrt(2*g*fluid_column_height)
co = 10.0 * vref
nu = 0.0
tf = 3.0
p0 = ro*co**2


class Dambreak2D(DB.DamBreak2D):

    def create_particles(self):
        xt, yt = get_2d_tank(dx=self.dx, length=container_width,
                             height=container_height, num_layers=4)
        xf, yf = get_2d_block(dx=self.dx, length=fluid_column_width,
                              height=fluid_column_height, center=[-1.5, 1])
        xt += 2.0
        xf += 2.0
        xf += self.dx
        yf += self.dx
        h = self.hdx * self.dx
        m = self.dx**2 * ro
        fluid = get_particle_array(name='fluid', x=xf, y=yf, h=h, m=m, rho=ro)
        tank = get_particle_array(name='boundary', x=xt, y=yt, h=h, m=m,
                                  rho=ro)
        self.scheme.setup_properties([fluid, tank])
        return [fluid, tank]

    def consume_user_options(self):
        self.options.scheme = 'sisph'
        super(Dambreak2D, self).consume_user_options()

    def configure_scheme(self):
        dt = 0.125*self.h/vref
        print("dt = %f" % dt)
        self.scheme.configure_solver(
            dt=dt, tf=tf, adaptive_timestep=False, pfreq=10,
            output_at_times=[0.4, 1.0, 1.4, 1.8, 1.945, 2.2]
        )

    def create_scheme(self):
        scheme = SISPHScheme(
            fluids=['fluid'], solids=['boundary'], dim=2, nu=nu,
            c0=co, rho0=ro, alpha=0.05, gy=-g, pref=p0, internal_flow=False,
            hg_correction=True
        )
        return scheme

    def customize_output(self):
        self._mayavi_config('''
        b = particle_arrays['fluid']
        b.scalar = 'vmag'
        ''')


if __name__ == '__main__':
    app = Dambreak2D()
    app.run()
    app.post_process(app.info_filename)
