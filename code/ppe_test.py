import os
from argparse import ArgumentParser
from numpy import pi, sin, mgrid
import matplotlib.pyplot  as plt

from compyle.api import declare, get_config
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.base.nnps import DomainManager
from pysph.base.kernels import QuinticSpline
from pysph.sph.equation import Group, Equation
from pysph.sph.basic_equations import SummationDensity
from pysph.solver.output import dump

from sisph import (get_particle_array_sisph, PressureCoeffMatrix,
                   PressureCoeffMatrixIterativePerf, EvaluateKappa,
                   FreeSurfaceCorrection, SolveMatrix, FreeSurfaceBCMatrix,
                   UpdateGhostPressure,
                   ComputeOdiagP, FreeSurfaceBC, PPESolve)


class MakeDensityConstant(Equation):
    def __init__(self, dest, sources, rho0):
        self.rho0 = rho0
        super(MakeDensityConstant, self).__init__(dest, sources)

    def initialize(self, d_rho, d_idx):
        d_rho[d_idx] = self.rho0


def matrix_eqns(fluid, nop, free_surf=False, rho0=1.0, rho_cutoff=0.95,
                tol=1e-8, periodic=False, const_density=False):
    fname = fluid.name
    every = [fname]

    solver_eqns = []
    if not const_density:
        solver_eqns.append(
            Group(equations=[SummationDensity(dest=fname, sources=every)])
        )
    solver_eqns.append(
        Group(equations=[
            PressureCoeffMatrix(dest=fname, sources=every),
            EvaluateKappa(dest=fname, sources=every)
        ])
    )
    eq = []
    if not periodic:
        if free_surf:
            eq.append(
                FreeSurfaceCorrection(dest=fname, sources=every, rho0=rho0,
                                      free_surf_cutoff=rho_cutoff)
            )
        else:
            eq.append(
                FreeSurfaceBCMatrix(dest=fname, sources=every, rho0=rho0,
                                    rho_cutoff=rho_cutoff)
            )
        solver_eqns.append(Group(equations=eq))

    eq = []
    eq.append(
        SolveMatrix(dest=fname, sources=every, tol=tol)
    )
    solver_eqns.append(Group(equations=eq))
    return solver_eqns


def jacobi_eqns(fluid, nop, free_surf=False, rho0=1.0, rho_cutoff=0.95,
                tol=1e-8, omega=0.5, max_iterations=1000, periodic=False,
                const_density=False):
    fname = fluid.name
    every = [fname]

    stg = []
    if not const_density:
        stg.append(
            Group(equations=[SummationDensity(dest=fname, sources=every)])
        )
    stg.append(
        Group(equations=[
            PressureCoeffMatrixIterativePerf(dest=fname, sources=every),
            EvaluateKappa(dest=fname, sources=every)
        ])
    )

    solver_eqns = []
    if not periodic:
        eqns = []
        if free_surf:
            if not const_density:
                eqns.append(
                    Group(equations=[
                        SummationDensity(dest=fname, sources=every)
                    ])
                )
            eqns.append(
                FreeSurfaceCorrection(
                    dest=fname, sources=None, rho0=rho0,
                    free_surf_cutoff=rho_cutoff
                )
            )
        else:
            eqns.append(
                FreeSurfaceBC(
                    dest=fname, sources=None, rho0=rho0,
                    rho_cutoff=rho_cutoff
                )
            )
        solver_eqns.append(Group(equations=eqns))
    else:
        eqns = []
        eqns.append(
            UpdateGhostPressure(dest=fname, sources=None)
        )
        solver_eqns.append(Group(equations=eqns, real=False))

    eqns = []
    eqns.append(
        PPESolve(
            dest=fname, sources=every, tolerance=tol,
            omega=omega, max_iterations=max_iterations,
        )
    )
    solver_eqns.append(Group(equations=eqns))
    if const_density:
        eqns.append(
            Group(equations=[
                MakeDensityConstant(dest=fname, sources=every, rho0=rho0)
            ])
        )

    eqns = [
        ComputeOdiagP(dest=fname, sources=every)
    ]
    solver_eqns.append(Group(equations=eqns))

    stg.append(
        Group(
            equations=solver_eqns, iterate=True,
            max_iterations=max_iterations,
            min_iterations=min(max_iterations, 2)
        )
    )
    print(stg)
    return stg


def sph_eval(parrays, equations, kernel, dim, domain_manager):
    return SPHEvaluator(
        arrays=parrays, equations=equations, dim=dim, kernel=kernel,
        domain_manager=domain_manager
    )


def create_domain(xmin, xmax, ymin, ymax, periodic_in_x, periodic_in_y):
    return DomainManager(
        xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, periodic_in_x=periodic_in_x,
        periodic_in_y=periodic_in_y
    )


if __name__ == '__main__':
    get_config().use_openmp = True

    p = ArgumentParser()
    p.add_argument('--perturb', action='store', type=float, dest='perturb',
                   default=0.0, help='Perturb the particles.')
    p.add_argument('-d', action='store', type=str, dest='filename',
                   default='solver_accuracy', help='Folder name.')
    p.add_argument('--max-iters', action='store', type=int, dest='max_iters',
                   default=20000, help='Max iterations.')
    p.add_argument('--dx', action='store', type=float, dest='dx',
                   default=0.01, help='Particle spacing.')
    p.add_argument('--hdx', action='store', type=float, dest='hdx',
                   default=1.0, help='hdx for the smoothing length.')
    p.add_argument('--tol', action='store', type=float, dest='tol',
                   default=1e-3, help='Tolerance for the Jacobi solver.')
    p.add_argument('--omega', action='store', type=float, dest='omega',
                   default=0.5, help='Omega for the Jacobi solver.')
    p.add_argument('--free-surf', action='store_true', dest='free_surf',
                   default=False,
                   help='Use semi-analytical free surface bcs.')
    p.add_argument('--periodic', action='store_true', dest='periodic',
                   default=False, help='Use periodic bcs.')
    p.add_argument('--constant-density', action='store_true',
                   dest='const_density', default=False,
                   help='Use constant density.')

    args = p.parse_args()

    dx = args.dx
    rho = 1.0
    m = dx*dx*rho
    if args.periodic:
        x, y = mgrid[dx/2:1:dx, dx/2:1:dx]
    else:
        x, y = mgrid[0:1+dx:dx, 0:1+dx:dx]
    dim = 2
    hdx = args.hdx
    rho0 = 1.0
    xmin, xmax, ymin, ymax = 0, 1, 0, 1

    rhs = -8*pi**2*sin(2*pi*x)*sin(2*pi*y)

    pa = get_particle_array_sisph(
        name='fluid', x=x, y=y, rhs=rhs, h=hdx*dx, m=m, rho=rho, dx=dx
    )

    nop = len(pa.x)
    pa.add_constant('nop', [nop])
    pa.add_constant('no_of_sources', [1])
    pa.add_property('offset', stride=1, type='int')
    pa.add_property('coeffs', stride=len(pa.x))
    pa.add_constant('source_idx', [0])
    kernel = QuinticSpline(dim=2)
    if args.periodic:
        domain_manager = create_domain(xmin, xmax, ymin, ymax,
                                       periodic_in_x=True, periodic_in_y=True)
    else:
        domain_manager = None

    p_exact = sin(2*pi*x)*sin(2*pi*y)
    p_exact = p_exact.ravel()
    pa.add_property('p_ext')
    pa.p_ext[:] = p_exact

    filename = args.filename
    os.makedirs(filename, exist_ok=True)
    dump(os.path.join(filename, 'diffision_0'), [pa],
         dict(t=0, dt=0.1, count=0), detailed_output=True, only_real=False)

    eqns = matrix_eqns(pa, nop, free_surf=args.free_surf,
                       periodic=args.periodic)
    compute_p_matrix = sph_eval([pa], eqns, kernel, dim, domain_manager)
    compute_p_matrix.evaluate(dt=0.1)
    pa.add_property('p_mat')
    pa.p_mat[:] = pa.p

    plt.scatter(pa.x, pa.y, c=abs(p_exact-pa.p_mat))
    plt.title('BiCGStab')
    plt.colorbar()
    plt.show()

    # Reset inital pk, p to zero
    pa.pk[:] = 0.0
    pa.p[:] = 0.0

    eqns = jacobi_eqns(
        pa, nop, free_surf=args.free_surf, rho0=rho0,
        tol=args.tol, max_iterations=args.max_iters,
        omega=args.omega, periodic=args.periodic
    )

    compute_p_jacobi = sph_eval([pa], eqns, kernel, dim, domain_manager)
    compute_p_jacobi.evaluate()
    pa.add_property('p_jac')
    pa.p_jac[:] = pa.p

    dump(os.path.join(filename, 'diffusion_1'), [pa],
         dict(t=0, dt=0.1, count=0), detailed_output=True, only_real=True)

    plt.scatter(pa.x, pa.y, c=abs(p_exact-pa.p_jac))
    plt.title('Jacobi')
    plt.colorbar()
    plt.show()
