"""
Flow past cylinder
"""
import os
import numpy as np

from pysph.sph.equation import Equation
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.tools import geometry as G
from pysph.sph.bc.inlet_outlet_manager import (
    InletInfo, OutletInfo)
from pysph.sph.scheme import add_bool_argument

# local files
from sisph import SISPHScheme

# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


class SolidWallNoSlipBCReverse(Equation):
    def __init__(self, dest, sources, nu):
        self.nu = nu
        super(SolidWallNoSlipBCReverse, self).__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_m, d_rho, s_rho, d_V, s_V,
             d_ug, d_vg, d_wg,
             d_au, d_av, d_aw,
             s_u, s_v, s_w,
             DWIJ, R2IJ, EPS, XIJ):

        etai = self.nu * d_rho[d_idx]
        etaj = self.nu * s_rho[s_idx]

        etaij = 2 * (etai * etaj)/(etai + etaj)

        Vi = 1./d_V[d_idx]
        Vj = 1./s_V[s_idx]
        Vi2 = Vi * Vi
        Vj2 = Vj * Vj

        # scalar part of the kernel gradient
        Fij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]

        # viscous contribution (third term) from Eq. (8), with VIJ
        # defined appropriately using the ghost values
        tmp = 1./d_m[d_idx] * (Vi2 + Vj2) * (etaij * Fij/(R2IJ + EPS))

        d_au[d_idx] += tmp * (d_ug[d_idx] - s_u[s_idx])
        d_av[d_idx] += tmp * (d_vg[d_idx] - s_v[s_idx])
        d_aw[d_idx] += tmp * (d_wg[d_idx] - s_w[s_idx])


class ExtrapolateUhat(Equation):
    def initialize(self, d_idx, d_uhat, d_wij):
        d_uhat[d_idx] = 0.0
        d_wij[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_uhat, s_u, d_wij, s_rho,
             d_au, d_av, d_aw, WIJ, XIJ):
        d_uhat[d_idx] += s_u[s_idx]*WIJ
        d_wij[d_idx] += WIJ

    def post_loop(self, d_idx, d_wij, d_uhat, d_rho):
        if d_wij[d_idx] > 1e-14:
            d_uhat[d_idx] /= d_wij[d_idx]
        else:
            d_uhat[d_idx] = 1.0


class ResetInletVelocity(Equation):
    def __init__(self, dest, sources, U, V, W):
        self.U = U
        self.V = V
        self.W = W

        super(ResetInletVelocity, self).__init__(dest, sources)

    def loop(self, d_idx, d_u, d_v, d_w, d_x, d_y, d_z, d_xn, d_yn, d_zn):
        d_u[d_idx] = self.U
        d_v[d_idx] = self.V
        d_w[d_idx] = self.W


class WindTunnel(Application):
    def initialize(self):
        # Geometric parameters
        self.Lt = 30.0  # length of tunnel
        self.Wt = 15.0  # half width of tunnel
        self.dc = 1.2  # diameter of cylinder
        self.cxy = 10., 0.0  # center of cylinder
        self.nl = 10  # Number of layers for wall/inlet/outlet

    def add_user_options(self, group):
        group.add_argument(
            "--re", action="store", type=float, dest="re", default=200,
            help="Reynolds number."
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.2,
            help="Ratio h/dx."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=20,
            help="Number of points in 1D of the cylinder."
        )
        group.add_argument(
            "--lt", action="store", type=float, dest="Lt", default=30,
            help="Length of the WindTunnel."
        )
        group.add_argument(
            "--wt", action="store", type=float, dest="Wt", default=15,
            help="Half width of the WindTunnel."
        )
        group.add_argument(
            "--dc", action="store", type=float, dest="dc", default=2.0,
            help="Diameter of the cylinder."
        )
        add_bool_argument(
            group, 'potential', dest='potential', default=True,
            help='Initialize with potential flow.'
        )

    def consume_user_options(self):
        self.Lt = self.options.Lt
        self.Wt = self.options.Wt
        self.dc = self.options.dc
        nx = self.options.nx
        re = self.options.re

        self.nu = nu = umax * self.dc / re
        # self.cxy = self.Lt / 5., 0.0

        self.dx = dx = self.dc / nx
        self.volume = dx * dx
        hdx = self.options.hdx
        self.nl = (int)(10.0*hdx)

        self.h = h = hdx * self.dx
        dt_cfl = 0.125 * h / (2 * umax)
        dt_viscous = 0.125 * h**2 / nu

        self.dt = min(dt_cfl, dt_viscous)
        self.tf = 200.0

    def _create_fluid(self):
        dx = self.dx
        h0 = self.h
        x, y = np.mgrid[dx / 2:self.Lt:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        fluid = get_particle_array(
            name='fluid', m=m, x=x, y=y, h=h0, V=1.0 / volume, u=umax,
            p=0.0, rho=rho, vmag=0.0)
        xc, yc = self.cxy
        xnew = x - xc
        ynew = y - yc
        znew = xnew + 1j*ynew
        a2 = (self.dc * 0.5)**2
        vel = umax - (a2/(znew*znew)).conjugate()
        if self.options.potential:
            fluid.u[:] = vel.real
            fluid.v[:] = vel.imag
            fluid.vmag[:] = np.abs(vel)
            # fluid.p[:] = 10000. - 0.5 * rho * np.abs(vel)**2
        return fluid

    def _create_solid(self):
        dx = self.dx
        h0 = self.h
        x = []
        y = []
        r = dx/2
        nt = 0
        while r < self.dc / 2:
            nnew = int(np.pi*r**2/dx**2 + 0.5)
            tomake = nnew-nt
            theta = np.linspace(0., 2.*np.pi, tomake + 1)
            for t in theta[:-1]:
                x.append(r*np.cos(t))
                y.append(r*np.sin(t))
            nt = nnew
            r = r + dx
        x = np.array(x)
        y = np.array(y)
        x, y = (t.ravel() for t in (x, y))
        x += self.cxy[0]
        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y,
            m=volume*rho, rho=rho, h=h0, V=1.0/volume)
        return solid

    def _create_solid1(self):
        dx = self.dx
        h0 = self.h
        x, y = np.mgrid[dx / 2:self.Lt:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        xc, yc = self.cxy
        cond = (x - xc)**2 + (y - yc)**2 < (self.dc/2*self.dc/2)
        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x[cond].ravel(), y=y[cond].ravel(), m=volume*rho,
            rho=rho, h=h0, V=1.0/volume
        )
        return solid

    def _create_wall(self):
        dx = self.dx
        h0 = self.h
        x0, y0 = np.mgrid[
            dx/2: self.Lt+self.nl*dx+self.nl*dx: dx, dx/2: self.nl*dx: dx]
        x0 -= self.nl*dx
        y0 -= self.nl*dx+self.Wt
        x0 = np.ravel(x0)
        y0 = np.ravel(y0)

        x1 = np.copy(x0)
        y1 = np.copy(y0)
        y1 += self.nl*dx+2*self.Wt
        x1 = np.ravel(x1)
        y1 = np.ravel(y1)

        x0 = np.concatenate((x0, x1))
        y0 = np.concatenate((y0, y1))
        volume = dx*dx
        wall = get_particle_array(
            name='wall', x=x0, y=y0, m=volume*rho, rho=rho, h=h0,
            V=1.0/volume)
        return wall

    def _set_wall_normal(self, pa):
        props = ['xn', 'yn', 'zn']
        for p in props:
            pa.add_property(p)

        y = pa.y
        cond = y > 0.0
        pa.yn[cond] = 1.0
        cond = y < 0.0
        pa.yn[cond] = -1.0

    def _create_outlet(self):
        dx = self.dx
        h0 = self.h
        nl = self.nl
        x, y = np.mgrid[dx/2:nl * dx:dx,  -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        x += self.Lt
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        outlet = get_particle_array(
            name='outlet', x=x, y=y, m=m, h=h0, V=1.0/volume, u=umax,
            p=0.0, rho=one * rho, uhat=0.0, vhat=0.0)
        xc, yc = self.cxy
        xnew = x - xc
        ynew = y - yc
        znew = xnew + 1j*ynew
        a2 = (self.dc * 0.5)**2
        vel = umax - (a2/(znew*znew)).conjugate()
        outlet.u[:] = vel.real
        outlet.v[:] = vel.imag
        outlet.uhat[:] = vel.real
        outlet.vhat[:] = vel.imag
        # outlet.p[:] = 10000. - 0.5 * rho * np.abs(vel)**2
        return outlet

    def _create_inlet(self):
        dx = self.dx
        h0 = self.h
        nl = self.nl
        x, y = np.mgrid[dx / 2:nl*dx:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        x = x - nl * dx
        one = np.ones_like(x)
        volume = one * dx * dx

        inlet = get_particle_array(
            name='inlet', x=x, y=y, m=volume * rho, h=h0, u=umax, rho=rho,
            V=1.0 / volume, p=0.0)
        xc, yc = self.cxy
        xnew = x - xc
        ynew = y - yc
        znew = xnew + 1j*ynew
        a2 = (self.dc * 0.5)**2
        vel = umax - (a2/(znew*znew)).conjugate()
        inlet.u[:] = vel.real
        inlet.v[:] = vel.imag
        # inlet.p[:] = 10000. - 0.5 * rho * np.abs(vel)**2
        return inlet

    def create_particles(self):
        dx = self.dx
        fluid = self._create_fluid()
        solid = self._create_solid()
        G.remove_overlap_particles(fluid, solid, dx, dim=2)
        outlet = self._create_outlet()
        inlet = self._create_inlet()
        wall = self._create_wall()

        particles = [fluid, inlet, outlet, solid, wall]
        self.scheme.setup_properties(particles)

        self._set_wall_normal(wall)

        return particles

    def create_scheme(self):
        nu = None
        s = SISPHScheme(
            ['fluid'], ['solid'], dim=2, rho0=rho, c0=c0,
            nu=nu, alpha=0.0, inlet_outlet_manager=None,
            inviscid_solids=['wall'], pref=p0, internal_flow=True,
            rho_cutoff=0.2, symmetric=True,
            gtvf=True, tolerance=1e-2, use_pref=True
        )
        return s

    def configure_scheme(self):
        scheme = self.scheme
        self.iom = self._create_inlet_outlet_manager()
        scheme.inlet_outlet_manager = self.iom
        pfreq = 50
        self.iom.update_dx(self.dx)
        scheme.configure(nu=self.nu)

        scheme.configure_solver(tf=self.tf, dt=self.dt,
                                pfreq=pfreq, n_damp=0)

    def _get_io_info(self):
        from sisph_inlet_outlet import SISPHInletOutlet

        inleteqns = [
            ResetInletVelocity('inlet', [], U=umax, V=0.0, W=0.0),
            ExtrapolateUhat('outlet', ['fluid'])
        ]

        props_to_copy=['x', 'y', 'z', 'u', 'v', 'w', 'uhat', 'vhat', 'what',
                       'p', 'h', 'rho', 'm']
        outleteqns = None
        manager = SISPHInletOutlet

        inlet_info = InletInfo(
            pa_name='inlet', normal=[-1.0, 0.0, 0.0],
            refpoint=[0.0, 0.0, 0.0], equations=inleteqns,
            has_ghost=False
        )

        outlet_info = OutletInfo(
            pa_name='outlet', normal=[1.0, 0.0, 0.0],
            refpoint=[self.Lt, 0.0, 0.0], equations=outleteqns,
            has_ghost=False, props_to_copy=props_to_copy
        )

        return inlet_info, outlet_info, manager

    def _create_inlet_outlet_manager(self):
        inlet_info, outlet_info, manager = self._get_io_info()
        iom = manager(
            fluid_arrays=['fluid'], inletinfo=[inlet_info],
            outletinfo=[outlet_info]
        )
        return iom

    def create_inlet_outlet(self, particle_arrays):
        iom = self.iom
        io = iom.get_inlet_outlet(particle_arrays)
        return io

    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        self.res = os.path.join(self.output_dir, 'results.npz')
        t, cd, cl = self._plot_force_vs_t()
        np.savez(self.res, t=t, cd=cd, cl=cl)

    def _plot_force_vs_t(self):
        from pysph.solver.utils import iter_output, load
        from pysph.tools.sph_evaluator import SPHEvaluator
        from pysph.sph.equation import Group
        from pysph.base.kernels import QuinticSpline
        from pysph.sph.wc.transport_velocity import (
            MomentumEquationPressureGradient,
            SummationDensity, SetWallVelocity
        )


        data = load(self.output_files[0])
        solid = data['arrays']['solid']
        fluid = data['arrays']['fluid']

        prop = ['awhat', 'auhat', 'avhat', 'wg', 'vg', 'ug', 'V', 'uf', 'vf',
                'wf', 'wij', 'vmag', 'pavg', 'nnbr']
        for p in prop:
            solid.add_property(p)
            fluid.add_property(p)

        # We find the force of the solid on the fluid and the opposite of that
        # is the force on the solid. Note that the assumption is that the solid
        # is far from the inlet and outlet so those are ignored.
        print(self.nu, p0, self.dc, rho)
        equations = [
            Group(
                equations=[
                    SummationDensity(dest='fluid', sources=['fluid', 'solid']),
                    SummationDensity(dest='solid', sources=['fluid', 'solid']),
                    SetWallVelocity(dest='solid', sources=['fluid']),
                    ], real=False),
            Group(
                equations=[
                    # Pressure gradient terms
                    MomentumEquationPressureGradient(
                        dest='solid', sources=['fluid'], pb=p0),
                    SolidWallNoSlipBCReverse(
                        dest='solid', sources=['fluid'], nu=self.nu),
                    ], real=True),
        ]
        sph_eval = SPHEvaluator(
            arrays=[solid, fluid], equations=equations, dim=2,
            kernel=QuinticSpline(dim=2)
        )
        t, cd, cl = [], [], []
        import gc
        print(self.dc, self.dx, self.nu)

        it = 0
        if os.path.exists(self.res):
            results = np.load(self.res)
            t, cl, cd = map(list, [results['t'], results['cl'], results['cd']])
            it = len(self.output_files)
            for sd, _ in iter_output(self.output_files[::-1]):
                it -= 1
                if abs(t[-1] - sd['t']) < 1e-6:
                    it -= 1
                    break

        for sd, arrays in iter_output(self.output_files[it:]):
            fluid = arrays['fluid']
            solid = arrays['solid']
            for p in prop:
                solid.add_property(p)
                fluid.add_property(p)
            t.append(sd['t'])
            sph_eval.update_particle_arrays([solid, fluid])
            sph_eval.evaluate()
            fx = solid.m*solid.au
            fy = solid.m*solid.av
            cd.append(np.sum(fx)/(0.5 * rho * umax**2 * self.dc))
            cl.append(np.sum(fy)/(0.5 * rho * umax**2 * self.dc))
            print('cd, cl ', np.sum(fx), np.sum(fy), cd[-1], cl[-1], t[-1])
            gc.collect()
        t, cd, cl = list(map(np.asarray, (t, cd, cl)))
        # Now plot the results.
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt
        plt.figure()
        plt.plot(t, cd, label=r'$C_d$')
        plt.plot(t, cl, label=r'$C_l$')
        plt.xlabel(r'$t$')
        plt.ylabel('cd/cl')
        plt.legend()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()
        return t, cd, cl

    def customize_output(self):
        self._mayavi_config('''
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        ''')

    def post_step(self, solver):
        freq = 500
        if solver.count % freq == 0:
            self.nnps.update()
            for i, pa in enumerate(self.particles):
                if pa.name == 'fluid':
                    self.nnps.spatially_order_particles(i)
            self.nnps.update()



if __name__ == '__main__':
    app = WindTunnel()
    app.run()
    app.post_process(app.info_filename)
