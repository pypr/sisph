import numpy
from compyle.api import declare

from pysph.sph.equation import Equation, Group

from sisph import (UpdateGhostPressure, PressureCoeffMatrixIterative,
                   PressureCoeffMatrixIterativePerf, EvaluateNumberDensity,
                   FreeSurfaceBC, SetPressureSolid)

from mat_coeff import (ComputeAConj, ComputeAS, ComputeAConjPerf, ComputeASPerf,
                       ProjectConj, ProjectS)


class InitializeBiCGStab(Equation):
    def initialize(self, d_idx, d_precond, d_diag):
        # Diags are going to zero. Remember to divide the rhs.
        # d_precond[d_idx] = d_diag[d_idx]
        d_precond[d_idx] = 1.0

    def post_loop(self, d_idx, d_residue, d_pk, d_AX, d_diag, d_odiag_pk, d_rhs,
                  d_q, d_Conj, d_precond, d_residue_hat, d_rho):
        d_AX[d_idx] = (d_diag[d_idx]*d_pk[d_idx] + d_odiag_pk[d_idx])

        d_residue[d_idx] = d_rhs[d_idx] - d_AX[d_idx]
        d_residue_hat[d_idx] = d_residue[d_idx]

        d_Conj[d_idx] = d_residue[d_idx]


class ComputeResNorm(Equation):
    def reduce(self, dst, dt, t):
        dst.res_norm[0] = numpy.sum(dst.residue_hat*dst.residue)
        if abs(dst.res_norm[0]) < 1e-16:
            print("The method fails. Since, norm of residue is zero!")


class SolvePreconditioner(Equation):
    def initialize(self, d_idx, d_Conj, d_precond, d_Conj_hat):
        d_Conj_hat[d_idx] = d_Conj[d_idx] / d_precond[d_idx]


class ComputeAlpha(Equation):
    def reduce(self, dst, dt, t):
        conj_norm, sconj_norm = declare('double', 2)

        conj_norm = numpy.sum(dst.residue_hat*dst.AConj)

        if abs(conj_norm) < 1e-12:
            conj_norm += 1e-12

        dst.alpha[0] = dst.res_norm[0]/conj_norm

        dst.S[:] = dst.residue - dst.alpha[0]*dst.AConj
        sconj_norm = numpy.sum(dst.S*dst.S)
        if sconj_norm < 1e-12:
            print("Cuppax: ", sconj_norm)
            dst.pk[:] = dst.pk + dst.alpha[0]*dst.Conj_hat
            dst.p[:] = dst.p + dst.alpha[0]*dst.Conj_hat


class SolvePreconditioner2(Equation):
    def initialize(self, d_idx, d_S, d_precond, d_S_hat):
        d_S_hat[d_idx] = d_S[d_idx] / d_precond[d_idx]


class ComputeOmega(Equation):
    def reduce(self, dst, t, dt):
        tmp1, tmp2 = declare('double', 2)

        tmp1 = numpy.sum(dst.AS*dst.S)
        tmp2 = numpy.sum(dst.AS*dst.AS)

        if tmp2 < 1e-12:
            tmp2 += 1e-12
        dst.omega[0] = tmp1/(tmp2 + 1e-12)
        if abs(dst.omega[0]) < 1e-12:
            print("Omega is zero. To continue omega must be non-zero")


class UpdateResidue(Equation):
    def __init__(self, dest, sources, rho0, rho_cutoff=0.8):
        self.rho0 = rho0
        self.rho_cutoff = rho_cutoff
        super(UpdateResidue, self).__init__(dest, sources)

    def initialize(self, d_idx, d_Conj_hat, d_pk, d_AConj, d_residue, d_q,
                   d_pdiff, d_alpha, d_precond, d_p, d_omega, d_S_hat,
                   d_AS, d_rho, d_S):
        alpha = d_alpha[0]
        omega = d_omega[0]

        d_pdiff[d_idx] = alpha * d_Conj_hat[d_idx] + omega*d_S_hat[d_idx]

        if d_rho[d_idx]/self.rho0 < self.rho_cutoff:
            d_pk[d_idx] = 0.0
            d_p[d_idx] = 0.0
            d_residue[d_idx] = 0.0
        else:
            d_pk[d_idx] = d_pk[d_idx] + d_pdiff[d_idx]
            d_p[d_idx] = d_pk[d_idx]
            d_residue[d_idx] = d_S[d_idx] - omega*d_AS[d_idx]


class CheckConvergence(Equation):
    def __init__(self, dest, sources, tolerance=0.05, max_iterations=1000):
        self.conv = 0
        self.tolerance = tolerance
        self.count = 0.0
        self.max_iterations = max_iterations
        super(CheckConvergence, self).__init__(dest, sources)

    def reduce(self, dst, dt, t):
        self.count += 1
        dst.iters[0] = self.count
        if dst.gpu is not None:
            from compyle.array import sum
            n = dst.gpu.res.length
            res_mean = sum(dst.gpu.res, dst.backend) / n
            self.conv = 1 if res_mean < self.tolerance else -1
        else:
            res_norm = numpy.sum(dst.residue*dst.residue)
            # print("Iters:", self.count, res_norm)
            self.conv = 1 if res_norm < self.tolerance else -1

    def converged(self):
        if self.conv == 1 and self.count < self.max_iterations:
            # print("Iters:", self.count)
            self.count = 0
        if self.count > self.max_iterations:
            self.count = 0
            print("Max iterations exceeded")
        return self.conv


class ComputeBeta(Equation):
    def reduce(self, dst, t, dt):
        tmp2, omega, alpha = declare('double', 3)

        tmp2 = numpy.sum(dst.residue_hat*dst.residue)

        omega = dst.omega[0]
        alpha = dst.alpha[0]

        dst.beta[0] = (tmp2/(dst.res_norm[0] + 1e-12))*(alpha/(omega + 1e-12))


class ComputeConjugate(Equation):
    def __init__(self, dest, sources, rho0, rho_cutoff=0.8):
        self.rho0 = rho0
        self.rho_cutoff = rho_cutoff
        super(ComputeConjugate, self).__init__(dest, sources)

    def post_loop(self, d_idx, d_Conj, d_residue, d_q, d_p, d_pabs, d_pk,
                  d_beta, d_AConj, d_omega, d_rho):
        beta = d_beta[0]
        omega = d_omega[0]

        fac = beta*(d_Conj[d_idx] - omega*d_AConj[d_idx])
        d_Conj[d_idx] = d_residue[d_idx] + fac


def bicgstab_equations(fluids, solids, inviscid_solids, iom, rho0, rho_cutoff,
                       tol, max_iters, debug=True, has_ghosts=False, gx=0.0,
                       gy=0.0, gz=0.0, hg_correction=False, perf=False):
    fluids_with_input = fluids
    all = fluids + solids + inviscid_solids

    if iom is not None:
        fluids_with_input += iom.get_io_names()
        fluids_with_input.remove('outlet')
        all += iom.get_io_names()

    # Should we do boundary conditions here?
    solver_eqns = []

    # Should this be fluids_with_input or must it also include output?
    if solids:
        eqs = []
        all_solids = solids + inviscid_solids
        for solid in all_solids:
            eqs.append(
                EvaluateNumberDensity(
                    dest=solid, sources=fluids_with_input
                )
            )
            eqs.append(
                SetPressureSolid(
                    dest=solid, sources=fluids_with_input, gx=gx, gy=gy, gz=gz,
                    hg_correction=hg_correction
                )
            )
        solver_eqns.append(Group(equations=eqs))

    eq = []
    for fluid in fluids_with_input:
        # Don't put this in the same group as IntializeCG because of
        # pre-conditioning in initialize method of InitializeBiCGStab.
        if perf:
            eq.append(
                PressureCoeffMatrixIterativePerf(dest=fluid, sources=all)
            )
        else:
            eq.append(
                PressureCoeffMatrixIterative(dest=fluid, sources=all)
            )
    solver_eqns.append(Group(equations=eq))

    eq = []
    for fluid in fluids_with_input:
        eq.append(
            FreeSurfaceBC(
                dest=fluid, sources=all, rho0=rho0, rho_cutoff=rho_cutoff
            )
        )
    solver_eqns.append(Group(equations=eq))


    eq = []
    for fluid in fluids_with_input:
        eq.append(
            InitializeBiCGStab(dest=fluid, sources=None)
        )
    solver_eqns.append(Group(equations=eq))

    eq, g2 = [], []
    if has_ghosts:
        ghost_eqns = Group(
            equations=[UpdateGhostPressure(dest=fluid, sources=None)
                       for fluid in fluids_with_input],
            real=False
        )
        g2.append(ghost_eqns)

    eq = []
    for fluid in fluids_with_input:
        eq.append(
            ComputeResNorm(dest=fluid, sources=None),
        )
    g2.append(Group(equations=eq))

    if solids:
        eqs = []
        all_solids = solids + inviscid_solids
        for solid in all_solids:
            eqs.append(
                ProjectConj(
                    dest=solid, sources=fluids_with_input
                )
            )
        g2.append(Group(equations=eqs))

    eq = []
    for fluid in fluids_with_input:
        eq.append(
            SolvePreconditioner(dest=fluid, sources=None),
        )
    g2.append(Group(equations=eq))

    eq = []
    for fluid in fluids_with_input:
        if perf:
            eq.append(
                ComputeAConjPerf(dest=fluid, sources=all)
            )
        else:
            eq.append(
                ComputeAConj(dest=fluid, sources=all, rho0=rho0,
                             rho_cutoff=rho_cutoff),
            )
        eq.append(
            ComputeAlpha(dest=fluid, sources=None)
        )
    g2.append(Group(equations=eq))

    if solids:
        eqs = []
        all_solids = solids + inviscid_solids
        for solid in all_solids:
            eqs.append(
                ProjectS(
                    dest=solid, sources=fluids_with_input
                )
            )
        g2.append(Group(equations=eqs))

    eq = []
    for fluid in fluids_with_input:
        eq.append(
            SolvePreconditioner2(dest=fluid, sources=None),
        )
    g2.append(Group(equations=eq))

    eq = []
    for fluid in fluids_with_input:
        if perf:
            eq.append(
                ComputeASPerf(dest=fluid, sources=all)
            )
        else:
            eq.append(
                ComputeAS(dest=fluid, sources=all, rho0=rho0,
                          rho_cutoff=rho_cutoff),
            )
        eq.append(
            ComputeOmega(dest=fluid, sources=None),
        )
    g2.append(Group(equations=eq))

    eq = []
    for fluid in fluids_with_input:
        eq.append(
            UpdateResidue(dest=fluid, sources=None, rho0=rho0,
                          rho_cutoff=rho_cutoff)
        )
        g2.append(Group(equations=eq))

    # Should this be fluids_with_input or must it also include output?
    if solids:
        eqs = []
        all_solids = solids + inviscid_solids
        for solid in all_solids:
            eqs.append(
                SetPressureSolid(
                    dest=solid, sources=fluids_with_input, gx=gx, gy=gy, gz=gz,
                    hg_correction=hg_correction
                )
            )
        g2.append(Group(equations=eqs))

    eq = []
    for fluid in fluids_with_input:
        eq.append(
            CheckConvergence(
                dest=fluid, sources=None, tolerance=tol,
                max_iterations=max_iters
            )
        )
    g2.append(Group(equations=eq))

    eq = []
    for fluid in fluids_with_input:
        eq.append(
            ComputeBeta(dest=fluid, sources=None)
        )
    g2.append(Group(equations=eq))

    eq = []
    for fluid in fluids_with_input:
        eq.append(
            ComputeConjugate(dest=fluid, sources=None, rho0=rho0,
                             rho_cutoff=rho_cutoff)
        )
    g2.append(Group(equations=eq))

    solver_eqns.append(
        Group(equations=g2, iterate=True, max_iterations=max_iters,
              min_iterations=1)
    )

    return solver_eqns
