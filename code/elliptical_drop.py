import numpy as np
from pysph.examples.elliptical_drop import EllipticalDrop as ED
from sisph import SISPHScheme


class EllipticalDrop(ED):
    def initialize(self):
        super(EllipticalDrop, self).initialize()
        self.hdx = 1.2
        self.vref = np.sqrt(2.0)*100
        self.co = self.vref*5.0
        self.alpha = 0.0
        self.p0 = self.ro*self.co**2

    def configure_scheme(self):
        dt = 0.25*self.hdx*self.dx/self.vref
        tf = 0.0076
        self.scheme.configure_solver(
            dt=dt, tf=tf, adaptive_timestep=False, pfreq=10,
            output_at_times=[0.0008, 0.0038]
        )

    def create_scheme(self):
        scheme = SISPHScheme(
            fluids=['fluid'], solids=[], dim=2, nu=0.0, rho0=self.ro,
            c0=self.co, alpha=self.alpha, pref=self.p0, internal_flow=False
        )
        return scheme

    def create_particles(self):
        [pa] = super(EllipticalDrop, self).create_particles()

        nfp = pa.get_number_of_particles()
        pa.gid[:] = np.arange(nfp)
        return [pa]


if __name__ == '__main__':
    app = EllipticalDrop()
    app.run()
    app.post_process(app.info_filename)
