"""
Poiseuille flow using the SISPH formulation.
"""
import numpy as np

from pysph.base.utils import get_particle_array
from pysph.examples.poiseuille import PoiseuilleFlow as PF

from sisph import SISPHScheme


class PoiseuilleFlow(PF):
    def initialize(self):
        self.d = 0.5
        self.Ly = 2*self.d
        self.Lx = 0.4*self.Ly
        self.rho0 = 1.0
        self.hdx = 1.0
        self.umax = 1.0

    def add_user_options(self, group):
        group.add_argument(
            "--re", action="store", type=float, dest="re", default=0.0125,
            help="Reynolds number of flow."
        )
        group.add_argument(
            "--nx", action="store", type=float, dest="nx", default=60,
            help="Number of particles."
        )
        group.add_argument(
            "--remesh", action="store", type=float, dest="remesh", default=0,
            help="Remeshing frequency (setting it to zero disables it)."
        )

    def consume_user_options(self):
        self.re = self.options.re
        self.dx = 1.0/self.options.nx
        self.nu = self.umax*self.Ly/(self.re)
        self.c0 = 10*self.umax
        self.p0 = self.c0**2*self.rho0

        # The body force is adjusted to give the Required Reynold's number
        # based on the steady state maximum velocity umax:
        # umax = fx/(2*nu)*(d^2) at the centerline

        self.fx = self.umax*2*self.nu/(self.d**2)
        h = self.hdx*self.dx
        # Setup default parameters.
        dt_cfl = 0.25 * h/(self.umax)
        dt_viscous = 0.25 * h**2/self.nu
        dt_force = 0.25 * np.sqrt(h/self.fx)

        self.dt = min(dt_cfl, dt_viscous, dt_force)
        print(dt_cfl, dt_viscous, dt_force)

    def configure_scheme(self):
        tf = 100.0
        scheme = self.scheme
        scheme.configure(c0=self.c0, gx=self.fx, pref=self.p0, nu=self.nu)
        scheme.configure_solver(tf=tf, dt=self.dt, pfreq=500)
        print("dt = %g"%self.dt)

    def create_scheme(self):
        s = SISPHScheme(
            ['fluid'], ['wall'], dim=2, rho0=self.rho0, c0=None, nu=None,
            gx=None, alpha=0.0, pref=None, internal_flow=True,
            rho_cutoff=0.4, domain=self.create_domain()
        )
        return s

    def create_particles(self):
        Lx = self.Lx
        Ly = self.Ly
        dx = self.dx
        _x = np.arange(dx/2, Lx, dx)

        # create the fluid particles
        _y = np.arange(dx/2, Ly, dx)

        x, y = np.meshgrid(_x, _y)
        fx = x.ravel()
        fy = y.ravel()

        # create the wall particles at the top
        ghost_extent = 5 * dx
        _y = np.arange(Ly+dx/2, Ly+dx/2+ghost_extent, dx)
        x, y = np.meshgrid(_x, _y); tx = x.ravel(); ty = y.ravel()

        # create the wall particles at the bottom
        _y = np.arange(-dx/2, -dx/2-ghost_extent, -dx)
        x, y = np.meshgrid(_x, _y); bx = x.ravel(); by = y.ravel()

        # concatenate the top and bottom arrays
        cx = np.concatenate( (tx, bx) )
        cy = np.concatenate( (ty, by) )

        # create the arrays
        wall = get_particle_array(name='wall', x=cx, y=cy)
        fluid = get_particle_array(name='fluid', x=fx, y=fy)

        print("Poiseuille flow :: Re = %g, nfluid = %d, nchannel=%d, umax=%d"%(
            self.re, fluid.get_number_of_particles(),
            wall.get_number_of_particles(), self.umax))

        # setup the particle properties
        volume = dx * dx

        # mass is set to get the reference density of rho0
        fluid.m[:] = volume * self.rho0
        wall.m[:] = volume * self.rho0

        # Set the default rho.
        fluid.rho[:] = self.rho0
        wall.rho[:] = self.rho0

        # smoothing lengths
        fluid.h[:] = self.hdx * dx
        wall.h[:] = self.hdx * dx

        # add requisite properties to the arrays:
        self.scheme.setup_properties([fluid, wall])

        # return the particle list
        return [fluid, wall]


if __name__ == '__main__':
    app = PoiseuilleFlow()
    app.run()
    app.post_process(app.info_filename)
