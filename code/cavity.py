"""Cavity with SISPH
"""
from pysph.examples.cavity import LidDrivenCavity as Cavity
from sisph import SISPHScheme


# domain and reference values
L = 1.0
Umax = 1.0
c0 = 10 * Umax
rho0 = 1.0
p0 = c0 * c0 * rho0


class CavitySISPH(Cavity):
    def consume_user_options(self):
        self.hdx = 1.0
        self.options.scheme = 'sisph'
        super(CavitySISPH, self).consume_user_options()

    def configure_scheme(self):
        dt = 0.125 * self.hdx * self.dx / Umax
        self.scheme.configure(nu=self.nu)
        self.scheme.configure_solver(tf=self.tf, dt=dt, pfreq=10)

    def create_scheme(self):
        scheme = SISPHScheme(
            ['fluid'], ['solid'], dim=2, nu=0.0, c0=c0, rho0=rho0, alpha=0.0,
            pref=p0, internal_flow=True, rho_cutoff=0.4

        )
        return scheme

    def create_particles(self):
        [fluid, solid] = super(CavitySISPH, self).create_particles()
        solid.u[solid.x > 1.0] = 0.0
        solid.u[solid.x < 0.0] = 0.0
        return [fluid, solid]

    def customize_output(self):
        self._mayavi_config('''
        fluid = particle_arrays['fluid']
        fluid.scalar = 'vmag'
        ''')


if __name__ == '__main__':
    app = CavitySISPH()
    app.run()
    app.post_process(app.info_filename)
