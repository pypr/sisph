from pysph.examples.two_blocks import TwoBlocks
from sisph import SISPHScheme


dx = 0.025
hdx = 1.0
rho0 = 1000
u0 = 1.0
c0 = 10*u0
p0 = rho0*c0**2


class TwoBlocksSISPH(TwoBlocks):
    def create_scheme(self):
        sisph = SISPHScheme(
            fluids=['fluid'], solids=[], dim=2, nu=0.1, rho0=rho0,
            c0=c0, alpha=0.0, pref=p0/10, internal_flow=False, gtvf=True,
            symmetric=True
        )
        return sisph

    def configure_scheme(self):
        dt = 2e-3
        tf = 1.0
        self.scheme.configure_solver(dt=dt, tf=tf, pfreq=1)

    def customize_output(self):
        self._mayavi_config('''
        b = particle_arrays['fluid']
        b.scalar = 'vmag'
        ''')




if __name__ == '__main__':
    app = TwoBlocksSISPH()
    app.run()
