#!/usr/bin/env python
import os
import json
import glob
from itertools import cycle, product

from automan.api import PySPHProblem as Problem
from automan.api import (Automator, Simulation, filter_cases,
                         compare_runs, filter_by_name)
import numpy as np
import matplotlib
from pysph.solver.utils import load, get_files
matplotlib.use('pdf')


n_core = 4
n_thread = 4
backend = ' --openmp'


def _get_cpu_time(case):
    info = glob.glob(case.input_path('*.info'))[0]
    with open(info) as fp:
        data = json.load(fp)
    return round(data['cpu_time'], 2)


def make_table(column_names, row_data, output_fname, sort_cols=None,
               **extra_kw):
    import pandas as pd
    col_data = [[] for i in range(len(column_names))]
    for row in row_data:
        for i, item in enumerate(row):
            col_data[i].append(item)
    d = {n: col_data[i] for i, n in enumerate(column_names)}
    df = pd.DataFrame(d, columns=column_names)
    if sort_cols:
        sort_by = [column_names[i] for i in sort_cols]
    else:
        sort_by = column_names[-1]
    df.sort_values(by=sort_by, inplace=True)
    parent = os.path.dirname(output_fname)
    if not os.path.exists(parent):
        os.mkdir(parent)
    with open(output_fname, 'w') as fp:
        fp.write(df.to_latex(index=False, escape=False, **extra_kw))


def get_files_at_given_times(files, times):
    result = []
    count = 0
    for f in files:
        data = load(f)
        t = data['solver_data']['t']
        if count >= len(times):
            break
        if abs(t - times[count]) < t*1e-8:
            result.append(f)
            count += 1
        elif t > times[count]:
            count += 1
    return result


def get_files_at_given_times_from_log(files, times, logfile):
    import re
    result = []
    time_pattern = r"output at time\ (\d+(?:\.\d+)?)"
    file_count, time_count = 0, 0
    with open(logfile, 'r') as f:
        for line in f:
            if time_count >= len(times):
                break
            t = re.findall(time_pattern, line)
            if t:
                if float(t[0]) in times:
                    result.append(files[file_count])
                    time_count += 1
                elif float(t[0]) > times[time_count]:
                    result.append(files[file_count])
                    time_count += 1
                file_count += 1
    return result


def merge_records(case_info):
    for key in case_info:
        info = case_info[key]
        if isinstance(info, tuple):
            if len(info) == 2:
                info, extra = case_info[key]
                info.update(extra)
            else:
                info = info[0]
            case_info[key] = info


def scheme_opts(params):
    if isinstance(params, tuple):
        return params[0]
    return params


class SquarePatch(Problem):
    def get_name(self):
        return 'square_patch'

    def setup(self):
        get_path = self.input_path
        nx = [100]
        cmd = 'python code/square_patch.py' + backend
        kernel = 'QuinticSpline'
        alpha = 0.15
        tol = 1e-3

        _case_info = {
            'wcsph': (dict(scheme='wcsph', alpha=alpha,
                           no_adaptive_timestep=None),
                      'WCSPH'),
            'sisph_tol_1e-3_gtvf': (dict(
                scheme='sisph', tol=tol, alpha=alpha, gtvf=None,
            ), fr"SISPH, asymm"),
            'sisph_tol_1e-3_symm_gtvf': (dict(
                scheme='sisph', tol=1e-3, alpha=alpha, symmetric=None, gtvf=None,
            ), fr"SISPH, symm")
        }

        self.case_info = {
            f'{s}_nx_{i}': dict(
                nx=i, kernel=kernel, **_case_info[s][0]
            )
            for s, i in product(_case_info, nx)
        }

        self.labels = {
            f'{s}_nx_{i}': _case_info[s][1]
            for s, i in product(_case_info, nx)
        }

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

        for s in self.case_info:
            if s.startswith('wcsph'):
                self.case_info[s]['tol'] = 'NA'

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._make_final_plot()
        self._make_table()

    def _make_table(self):
        column_names = ['Scheme', r'nx', r'$\epsilon$', 'CPU time (secs)']
        rows = []
        for ci in self.case_info.values():
            tol = str(ci['tol'])
            row = [ci['scheme'].upper(), ci['nx'],
                   tol, _get_cpu_time(ci['case'])]
            rows.append(row)
        make_table(
            column_names, rows, self.output_path(self.get_name() + '_time.tex'),
            sort_cols=[1, -1], column_format='lrrr'
        )

    def _make_final_plot(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        ids = ['a) ', 'b) ', 'c) ']
        fig, ax = plt.subplots(1, 3, figsize=(12, 3), dpi=300, sharex=True,
                               sharey=True)
        ax = ax.ravel()
        for j, name in enumerate(self.case_info):
            files = get_files(self.input_path(name), 'square_patch')
            data = load(files[-1])
            pa = data['arrays']['fluid']
            vmag = np.sqrt(pa.u**2 + pa.v**2)
            tmp = ax[j].scatter(pa.x, pa.y, marker='.', c=vmag,
                                vmin=min(vmag), vmax=max(vmag),
                                rasterized=True)
            plt.ylim(-2, 2)
            plt.xlim(plt.ylim())
            ax[j].set_xlabel(ids[j] + self.labels[name], fontsize=12)
        fig.colorbar(tmp, ax=ax.tolist(), shrink=0.95, label='vmag')
        plt.savefig(self.output_path(self.get_name() + '.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()


class EllipticalDrop(Problem):
    def get_name(self):
        return 'elliptical_drop'

    def setup(self):
        get_path = self.input_path
        nx = 50
        cmd = 'pysph run elliptical_drop' + backend
        sisph_cmd = 'python code/elliptical_drop.py' + backend
        kernel = 'QuinticSpline'
        tol = 1e-3

        self.case_info = {
            'wcsph': dict(
                scheme='wcsph', base_command=cmd, no_adaptive_timestep=None,
                kernel=kernel
            ),
            'sisph': (dict(base_command=sisph_cmd, tol=tol, kernel=kernel),
                      dict(scheme='sisph')),
            'sisph_gtvf_symm': (dict(base_command=sisph_cmd, tol=tol,
                                     kernel=kernel, gtvf=None, symmetric=None),
                                dict(scheme='sisph')),
            'sisph_gtvf': (dict(base_command=sisph_cmd, tol=tol, kernel=kernel,
                                gtvf=None),
                           dict(scheme='sisph')),
        }

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                nx=nx, cache_nnps=None, **scheme_opts(scheme)
            ) for name, scheme in self.case_info.items()
        ]
        merge_records(self.case_info)

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_axis_and_ke()
        self._plot_particles()
        self._make_table()

    def _make_table(self):
        column_names = ['Scheme', 'CPU time (secs)']
        rows = []
        for ci in self.case_info.values():
            row = [ci['scheme'].upper(), _get_cpu_time(ci['case'])]
            rows.append(row)
        make_table(
            column_names, rows, self.output_path(self.get_name() + '.tex'),
            column_format='lr'
        )

    def _plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files
        from pysph.examples.elliptical_drop import exact_solution

        linestyles = cycle(['k--', 'k-', 'k:'])
        for name in self.case_info:
            files = get_files(self.input_path(name), 'elliptical_drop')
            data = load(files[-1])
            pa = data['arrays']['fluid']
            _a, _A, _po, xe, ye = exact_solution(data['solver_data']['t'])
            plt.figure(figsize=(4, 8))
            plt.axis('equal')
            plt.plot(xe, ye, next(linestyles))
            plt.scatter(
                pa.x, pa.y, c=pa.p, marker='.', s=10,
                edgecolors='none', linewidth=0, cmap='jet'
            )
            plt.colorbar(label='p')
            plt.ylim(-2, 2)
            plt.xlabel('x')
            plt.ylabel('y')
            plt.savefig(self.output_path(name + '.pdf'),
                        bbox_inches='tight', pad_inches=0)
            plt.close()

    def _plot_axis_and_ke(self):
        import matplotlib.pyplot as plt

        data = {}
        linestyles = cycle(['k--', 'k:', 'k-'])
        for scheme in self.case_info:
            data[scheme] = np.load(self.input_path(scheme, 'results.npz'))

        plt.figure(figsize=(6, 5))
        for scheme in self.case_info:
            plt.plot(
                data[scheme]['t'],
                np.abs(data[scheme]['ymax'] - data[scheme]['major']),
                next(linestyles), label=scheme.upper()
            )
        plt.xlabel(r't')
        plt.ylabel('Error in major-axis')
        plt.legend(loc='upper left')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('major_axis.pdf'))
        plt.clf()
        plt.close()

        plt.figure(figsize=(6, 5))
        for scheme in self.case_info:
            plt.plot(
                data[scheme]['t'],
                np.abs(data[scheme]['ke'] - data[scheme]['major']),
                next(linestyles), label=scheme.upper()
            )
        plt.xlabel(r't')
        plt.ylabel('Kinetic Energy')
        plt.legend(loc='lower left')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('ke.pdf'))
        plt.close()


class Cavity(Problem):
    def get_name(self):
        return 'cavity'

    def setup(self):
        get_path = self.input_path
        nx = [50, 100, 200]
        tf = 10
        self.re = re = [100, 1000, 10000]
        tol = 1e-3

        cmd = 'pysph run cavity' + backend
        sisph_cmd = 'python code/cavity.py' + backend
        _case_info = {
            'sisph_gtvf_symm': (dict(base_command=sisph_cmd, pfreq=100,
                                     tol=tol, no_symmetric=None, gtvf=None,
                                     internal=None),
                                "SISPH"),
            'tvf': (dict(base_command=cmd, scheme='tvf',
                         no_adaptive_timestep=None),
                    "TVF")
        }

        self.case_info = {
            f'{s}_nx_{n}_re_{r}': dict(
                nx=n, re=r, tf=tf, **_case_info[s][0]
            )
            for s, n, r in product(_case_info.keys(), nx, re)
        }
        self.labels = {
            f'{s}_nx_{n}_re_{r}': _case_info[s][1] + f", nx = {n}"
            for s, n, r in product(_case_info.keys(), nx, re)
        }

        # Don't do nx 200 for low Re.
        for s in list(self.case_info):
            if (s.endswith('1000') or s.endswith('100')) and 'nx_200' in s:
                self.case_info.pop(s)

        for s in list(self.case_info):
            if s.endswith('1000'):
                self.case_info[s].update({'tf': 50})
            if s.endswith('10000'):
                self.case_info[s].update({'tf': 150})
                # TVF takes a lot of time to run, comment this line to run it.
                if s.startswith('tvf'):
                    self.case_info.pop(s)

        for s in list(self.case_info):
            if s.endswith('10000'):
                if 'nx_50' in s:
                    self.case_info.pop(s)

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **kwargs
            ) for name, kwargs in self.case_info.items()
        ]
        for s in self.case_info:
            if s.startswith('sisph'):
                self.case_info[s]['scheme'] = 'sisph'
        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_u_profile()
        self._make_table()

    def _make_table(self):
        column_names = ['Scheme', 'nx', 'Re', 'CPU time (secs)']
        rows = []
        for ci in self.case_info.values():
            row = [ci['scheme'].upper(), ci['nx'],
                   ci['re'], _get_cpu_time(ci['case'])]
            rows.append(row)
        make_table(
            column_names, rows, self.output_path(self.get_name() + '.tex'),
            sort_cols=[1, 2, -1], column_format='lrrr'
        )

    def _plot_u_profile(self):
        import matplotlib.pyplot as plt
        from pysph.examples.ghia_cavity_data import get_u_vs_y, get_v_vs_x

        # Re 100
        # u vs y
        y, exp_u = get_u_vs_y()
        x, exp_v = get_v_vs_x()

        for re in self.re:
            fig_u = plt.figure()
            fig_v = plt.figure()
            ax_u = fig_u.add_subplot(111)
            ax_v = fig_v.add_subplot(111)
            ax_u.plot(exp_u[re], y, 'ko', fillstyle='none',
                      label=f'Ghia et al. (Re={re:,})')
            ax_u.set_xlabel('u')
            ax_u.set_ylabel('y')
            ax_v.plot(x, exp_v[re], 'ko', fillstyle='none',
                      label=f'Ghia et al. (Re={re:,})')
            ax_v.set_xlabel('$x$')
            ax_v.set_ylabel('$v$')
            linestyles = cycle(['k-', 'k--', 'r:', 'r-.', 'b-.', 'b--'])
            for name in self.case_info:
                if name.endswith(str(re)):
                    data = np.load(self.input_path(name, 'results.npz'))

                    lstyle = next(linestyles)
                    ax_u.plot(data['u_c'], data['x'], lstyle,
                              label=self.labels[name])
                    ax_v.plot(data['x'], data['v_c'], lstyle,
                              label=self.labels[name])
            ax_u.set_aspect('equal', 'box')
            ax_v.set_aspect('equal', 'box')
            ax_u.legend(loc='best')
            ax_v.legend(loc='best')
            fig_u.savefig(self.output_path(f'u_re{re}.pdf'),
                          bbox_inches='tight', pad_inches=0)
            fig_v.savefig(self.output_path(f'v_re{re}.pdf'),
                          bbox_inches='tight', pad_inches=0)
            plt.close()


class DamBreak2D(Problem):
    def get_name(self):
        return 'dam_break_2d'

    def setup(self):
        get_path = self.input_path

        kernel = 'QuinticSpline'
        tol = 1e-2

        cmd = 'pysph run dam_break_2d' + backend
        sisph_cmd = 'python code/dam_break_2d.py' + backend

        _case_info = {
            'wcsph': (dict(
                base_command=cmd, scheme='wcsph',
                alpha=0.05, kernel=kernel, tf=1.0, dx=0.01
                ),
                      "WCSPH"),
            'edac': (dict(
                base_command=cmd, scheme='edac',
                alpha=0.05, kernel=kernel, tf=1.0, dx=0.01
                ),
                     "EDAC"),
            'sisph': (dict(base_command=sisph_cmd, alpha=0.05, tol=tol,
                           pfreq=100, kernel=kernel, gtvf=None, tf=2.5,
                           symmetric=None, dx=0.008),
                      "SISPH, symm")
        }

        self.case_info = {
            f'{s}': dict(**_case_info[s][0])
            for s in _case_info
        }

        self.labels = {f'{s}': _case_info[s][1] for s in _case_info}

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **scheme
            ) for name, scheme in self.case_info.items()
        ]
        for s in self.case_info:
            if s.startswith('sisph'):
                self.case_info[s]['scheme'] = 'sisph'

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_toe()
        self._plot_particles()
        self._make_table()

    def _make_table(self):
        column_names = ['Scheme', 'CPU time (secs)']
        rows = []
        for ci in self.case_info.values():
            row = [ci['scheme'].upper(), _get_cpu_time(ci['case'])]
            rows.append(row)
        make_table(
            column_names, rows, self.output_path(self.get_name() + '.tex'),
            column_format='lr'
        )

    def _plot_toe(self):
        import matplotlib.pyplot as plt
        from pysph.examples import db_exp_data as dbd

        linestyles = cycle(['k:', 'k--', 'k-', 'k-.'])
        data = {}
        plt.figure(figsize=(5, 4))
        for scheme in self.case_info:
            data[scheme] = np.load(self.input_path(scheme, 'results.npz'))
            plt.plot(
                data[scheme]['t'], data[scheme]['x_max'], next(linestyles),
                label=self.labels[scheme])
            tmps, xmps = dbd.get_koshizuka_oka_mps_data()
        plt.plot(tmps, xmps, '^', label='MPS Koshizuka & Oka (1996)')
        factor = np.sqrt(2.0*9.81/1.0)
        plt.xlim(0, 0.7*factor)
        plt.ylim(0.5, 4.5)
        plt.xlabel(r'$T$')
        plt.ylabel(r'$Z/L$')
        plt.legend(loc='upper left')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('toe_vs_t.pdf'))
        plt.close()

    def _plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        schemes = ['sisph']
        for name in schemes:
            fname = 'dam_break_2d'
            files = get_files(self.input_path(name), fname)
            times = [0.4, 1.8, 1.97, 2.2]
            logfile = os.path.join(self.input_path(name), 'dam_break_2d.log')
            to_plot = get_files_at_given_times_from_log(files, times, logfile)
            for i, f in enumerate(to_plot):
                print(i, f)
                data = load(f)
                pa = data['arrays']['fluid']
                for scalar in ('p',):
                    plt.figure(figsize=(5, 4), dpi=150)
                    plt.axis('equal')
                    if scalar == 'p':
                        c = pa.p
                        vmax = 15000
                    else:
                        c = np.sqrt(pa.u*pa.u + pa.v*pa.v)
                        vmax = None
                    plt.scatter(
                        pa.x, pa.y, c=c, marker='.', s=5,
                        edgecolors='none', cmap='jet', linewidth=0,
                        vmin=0, vmax=vmax, rasterized=True
                    )
                    plt.colorbar(label=scalar)
                    pa1 = data['arrays']['boundary']
                    plt.scatter(
                        pa1.x, pa1.y, c=pa1.m, marker='.', s=5,
                        edgecolors='none', linewidth=0, rasterized=True,
                    )
                    plt.annotate(
                        f't = {times[i]:.2f}', xy=(0.5, 3.5), fontsize=18
                    )
                    plt.xlim(-0.1, 4.1)
                    plt.ylim(-0.1, 4.1)
                    plt.xlabel('x')
                    plt.ylabel('y')
                    fbase = name + '_%s_%d' % (scalar, i)
                    plt.tight_layout(pad=0)
                    plt.savefig(self.output_path(fbase + '.pdf'))
                    plt.close()


class DamBreak3D(Problem):
    def get_name(self):
        return 'dam_break_3d'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/dam_break_3d.py' + backend
        tf = 2.0
        tol = 1e-2
        dx = 0.01

        self.case_info = {
            'sisph': (
                dict(base_command=cmd, tf=tf, pfreq=250, tol=tol,
                     symmetric=None, gtvf=None, dx=dx),
                dict(scheme='sisph', tol=tol)
            ),
        }
        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **scheme_opts(self.case_info[name])
            ) for name in self.case_info
        ]
        merge_records(self.case_info)
        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_particles()
        self._make_table()

    def _make_table(self):
        column_names = ['Scheme', r'$\epsilon$', 'CPU time (secs)']
        rows = []
        for ci in self.case_info.values():
            tol = str(ci['tol'])
            row = [ci['scheme'].upper(), tol, _get_cpu_time(ci['case'])]
            rows.append(row)
        make_table(
            column_names, rows, self.output_path(self.get_name() + '.tex'),
            column_format='lrr'
        )

    def _plot_particles(self):
        from pysph.solver.utils import get_files
        from mayavi import mlab
        mlab.options.offscreen = True

        for name in ('sisph',):
            fname = 'dam_break_3d'
            files = get_files(self.input_path(name), fname)
            times = [0.4, 1.3, 1.5, 1.6, 1.8, 2.0]
            logfile = os.path.join(self.input_path(name), 'dam_break_3d.log')
            to_plot = get_files_at_given_times_from_log(files, times, logfile)
            mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(800, 800))
            view = None
            for i, f in enumerate(to_plot):
                # print(i, f)
                data = load(f)
                t = data['solver_data']['t']
                print(i, f, t)
                f = data['arrays']['fluid']
                fg = mlab.points3d(
                    f.x, f.y, f.z, f.u, mode='point',
                    colormap='viridis', vmin=-2, vmax=5
                )
                fg.actor.property.render_points_as_spheres = True
                fg.actor.property.point_size = 3

                src = mlab.pipeline.builtin_surface()
                src.source = 'cube'
                src.data_source.trait_set(x_length=3.22,
                                          center=(1.61, 0.0, 0.5))
                s = mlab.pipeline.surface(src)
                s.actor.property.opacity = 0.1

                mlab.scalarbar(fg, title='u', orientation='horizontal')
                if view is None:
                    view = mlab.view()
                mlab.view(*view)
                mlab.text(0.7, 0.85, f"T = {times[i]} sec", width=0.2)

                opath = self.output_path(name + f'_u_{i}.png')
                mlab.savefig(opath)
                mlab.clf()


class TGV(Simulation):
    def __init__(self, root, base_command, job_info=None, depends=None,
                 label=None, **kw):
        self.label = label
        super(TGV, self).__init__(root, base_command, job_info, depends, **kw)

    def get_labels(self, dummy):
        return self.label

    def decay_exact(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        label = kw.pop('label', 'Exact')
        plt.semilogy(data['t'], data['decay_ex'], '-*', label=label, **kw)

    def decay(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.semilogy(data['t'], data['decay'], **kw)
        plt.xlabel('t')
        plt.ylabel('max velocity')

    def l1(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.plot(data['t'], data['l1'], **kw)
        plt.xlabel('t')
        plt.ylabel(r'$L_1$ error')

    def linf(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.plot(data['t'], data['linf'], **kw)
        plt.xlabel('t')
        plt.ylabel(r'$L_\infty$ error')

    def p_l1(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.plot(data['t'], data['p_l1'], **kw)
        plt.xlabel('t')
        plt.ylabel(r'$L_1$ error for $p$')


class TaylorGreen(Problem):
    def get_name(self):
        return "taylor_green"

    def setup(self):
        get_path = self.input_path
        kernel = 'QuinticSpline'
        tf = 5.0

        cmd = 'python code/taylor_green.py' + backend

        ########################################
        # Change tolerence, with GTVF and symm
        ########################################
        tols = [1e-1, 5e-2, 1e-2, 5e-3, 1e-3, 5e-4]
        re = 100
        nx = 100
        pfreq = 10
        perturb = 0.1
        max_iters = 20000

        self.case_info, self.labels = {}, {}
        for tol in tols:
            name = f'sisph_tol_{tol:.0e}'
            self.case_info.update({
                name: dict(
                    nx=nx, tol=tol, tf=tf, kernel=kernel, re=re,
                    perturb=perturb, base_command=cmd, gtvf=None,
                    no_symmetric=None, internal=None, perf=None,
                    max_iters=max_iters
                )
            })
            self.labels.update({name: fr"$\epsilon$: {tol:.0e}"})

        ########################################
        # Change GTVF w/o perturb, tol 5e-2
        ########################################
        tol = 1e-2
        re = 100
        nx = 100
        tf = 5.0
        perturb = 0.1
        pysph_cmd = "pysph run taylor_green.py" + backend

        self.case_info.update({
            f'isph_orig_shifting_no_gtvf': dict(
                nx=nx, tol=tol, tf=tf, kernel=kernel, re=re,
                base_command=pysph_cmd, shift_freq=1,
                shift_kind='simple', scheme='isph'
            ),
            'sisph_gtvf_no_symm': dict(
                nx=nx, tf=tf, kernel=kernel, base_command=cmd,
                tol=tol, gtvf=None, internal=None
            ),
        })

        self.labels.update({
            'isph_orig_shifting_no_gtvf': "Matrix - ISPH, Fickian smoothing",
            'sisph_gtvf_no_symm': "SISPH, asymm",
        })

        ########################################
        # Change Symm, w/o perturb, 5e-2
        ########################################
        tol = 1e-2
        re = 100
        nx = 100
        tf = 5.0
        self.case_info.update({
            'sisph_no_symmetric': dict(
                nx=nx, tol=tol, tf=tf, kernel=kernel, re=re,
                base_command=cmd, gtvf=None, no_symmetric=None
            ),
            'sisph_symmetric': dict(
                nx=nx, tol=tol, tf=tf, kernel=kernel, re=re,
                base_command=cmd, symmetric=None, gtvf=None
            ),
            'sisph_perturb_symmetric': dict(
                nx=nx, tf=tf, kernel=kernel, perturb=perturb, base_command=cmd,
                tol=tol, gtvf=None, symmetric=None, internal=None
            ),
        })

        self.labels.update({
            'sisph_no_symmetric': "asymm",
            'sisph_symmetric': "symm",
            'sisph_perturb_symmetric': f"symm, perturb = {perturb}",
        })

        ########################################
        # Different Re and nx
        ########################################
        res = [100, 1000]
        nxs = [50, 100]
        tol = 1e-2
        for nx, re in product(nxs, res):
            self.case_info.update({
                f'sisph_nx_{nx}_re_{re}': dict(
                    nx=nx, re=re, tf=tf, kernel=kernel,
                    base_command=cmd, gtvf=None, no_symmetric=None,
                    tol=tol, perf=None
                )})
            self.labels.update({
                f'sisph_nx_{nx}_re_{re}': 'SISPH, nx = {nx}, Re = {r}'
            })

        ########################################
        # Compare case_info
        ########################################
        pysph_cmd = 'pysph run taylor_green' + backend

        _case_info = {
            'wcsph': dict(scheme='wcsph', base_command=pysph_cmd,
                          no_adaptive_timestep=None, perturb=perturb),
            'edac': dict(scheme='edac', base_command=pysph_cmd,
                         no_adaptive_timestep=None, perturb=perturb),
            'iisph': dict(scheme='iisph', base_command=pysph_cmd,
                          no_adaptive_timestep=None, perturb=perturb),
            'sisph': dict(base_command=cmd, tol=1e-2, gtvf=None,
                          no_symmetric=None, perf=None),
        }

        kernel = 'QuinticSpline'
        perturb = 0.2
        tf = 2.5
        nx = 100
        re = 100
        self.case_info.update({
            f'{s}_scheme': dict(
                re=re, nx=nx, tf=tf, kernel=kernel,
                **_case_info[s]
            ) for s in _case_info
        })

        self.labels.update({
            f'{s}_scheme': f'{s.upper()}'
            for s in _case_info
        })

        self.cases = [
            TGV(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, label=self.labels[name], pfreq=50,
                **kwargs
            ) for name, kwargs in self.case_info.items()
        ]
        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        variants = [
            'tol', 'gtvf', 'symmetric', 'scheme'
        ]

        self._make_tables()
        self._plot_re()
        for variant in variants:
            path = self.output_path(variant)
            if not os.path.exists(path):
                os.mkdir(path)
            names = [x for x in self.case_info if variant in x]
            self._plot_all(variant, names=names)

    def _make_tables(self):
        # comparision with other schemes
        column_names = ['Scheme', 'CPU time (secs)']
        rows = []
        names = [x for x in self.case_info if x.endswith('scheme')]
        for name in names:
            ci = self.case_info[name]
            row = [name.split('_')[0].upper(), _get_cpu_time(ci['case'])]
            rows.append(row)
        make_table(
            column_names, rows,
            self.output_path('scheme', self.get_name() + '.tex'),
            sort_cols=[1], column_format='lr'
        )

        # Changes to tolerance.
        column_names = [r'$\epsilon$', 'CPU time (secs)']
        rows = []
        names = [x for x in self.case_info if x.startswith('sisph_tol')]
        for name in names:
            ci = self.case_info[name]
            row = ['%.1g' % ci['tol'], _get_cpu_time(ci['case'])]
            rows.append(row)
        make_table(
            column_names, rows,
            self.output_path('tol', self.get_name() + '.tex'),
            column_format='rr'
        )

    def _plot_re(self):
        import matplotlib.pyplot as plt

        os.mkdir(self.output_path('re'))
        names = [x for x in self.case_info if 're' in x]
        cases = filter_by_name(self.cases, names)

        fig1, ax1 = plt.subplots(figsize=(5, 4))
        fig2, ax2 = plt.subplots(figsize=(5, 4))
        for case in cases:
            re = case.params['re']
            nx = case.params['nx']
            data = np.load(case.input_path('results.npz'))

            ls = 'r' if nx == 50 else 'b'
            ls += '--' if nx == 50 else ':'
            lw = 1.0 if re == 100 else 1.5

            if re == 100:
                t_100 = data['t']
                decay_ex_100 = data['decay_ex']
            elif re == 1000:
                t_1k = data['t']
                decay_ex_1k = data['decay_ex']

            ax1.semilogy(data['t'], data['decay'], ls,
                         label=fr'$Re = {re}, nx={nx}$', linewidth=lw)
            ax2.plot(data['t'], data['p_l1'], ls,
                     label=fr'$Re = {re}, nx={nx}$', linewidth=lw)
        ax1.semilogy(t_100, decay_ex_100, 'k-', label=fr'Exact $Re=100$')
        ax1.semilogy(t_1k, decay_ex_1k, 'k-', label=fr'Exact $Re=1000$',
                     linewidth=1.0)
        ax1.legend(loc='best')
        ax2.legend(loc='best')
        ax1.set_xlabel('t')
        ax2.set_xlabel('t')
        ax1.set_ylabel('max velocity')
        ax2.set_ylabel(r'$p_{L_1}$ error')
        fig1.tight_layout(pad=0)
        fig2.tight_layout(pad=0)
        fig1.savefig(self.output_path('re', 'decay_all.pdf'))
        fig2.savefig(self.output_path('re', 'p_l1_error_all.pdf'))
        plt.close()

    def _plot_all(self, variant, names, ext=''):
        import matplotlib.pyplot as plt

        cases = filter_by_name(self.cases, names)
        size = (5, 4)
        plt.figure(figsize=size)
        compare_runs(cases, 'decay', labels=variant, exact='decay_exact')
        plt.legend(loc='best')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path(variant, ext + 'decay_all.pdf'))
        plt.close()

        plt.figure(figsize=size)
        compare_runs(cases, 'l1', labels=variant)
        plt.legend(loc='best')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path(variant, ext + 'l1_error_all.pdf'))
        plt.close()

        plt.figure(figsize=size)
        compare_runs(cases, 'linf', labels=variant)
        plt.legend(loc='best')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path(variant, ext + 'linf_error_all.pdf'))
        plt.close()

        plt.figure(figsize=size)
        compare_runs(cases, 'p_l1', labels=variant)
        plt.legend(loc='best')
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path(variant, ext + 'p_l1_error_all.pdf'))
        plt.close()

    def _plot_particles_2_by_3(self, cases, label='gtvf'):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        fig, ax = plt.subplots(2, 3, figsize=(8, 6), sharex=True, sharey=True)
        fig.subplots_adjust(wspace=0.2)
        ax = ax.T
        for i, case in enumerate(cases):
            files = get_files(case.input_path(), 'taylor_green')

            times = [0.2, 0.4, 1.0]
            files = get_files_at_given_times(files, times)

            for j, f in enumerate(files):
                data = load(f)
                f = data['arrays']['fluid']
                ax[i, j].scatter(
                    f.x, f.y, marker='.', s=3, c='k', edgecolors='none',
                    alpha=0.8, rasterized=True
                )
                # ax[i, j].set_xlim([0, 0.6])
                # ax[i, j].set_ylim([0.4, 1.0])
            ax[0, i].set_xlabel(r'$t = %.1f$' % times[i], fontsize=12)
        plt.tight_layout(pad=0)
        fig.savefig(self.output_path(label, 'comparision_vmag.pdf'))
        plt.close()


class TaylorGreenParticlePlots(Problem):
    def get_name(self):
        return "taylor_green_particle_plots"

    def setup(self):
        get_path = self.input_path
        kernel = 'QuinticSpline'

        pysph_cmd = 'pysph run taylor_green' + backend
        cmd = 'python code/taylor_green.py' + backend

        ########################################
        # Change GTVF w/o perturb, tol 5e-2
        ########################################
        tol = 1e-2
        re = 100
        nx = 50
        tf = 1.2

        self.case_info = {
            f'sisph_no_gtvf': dict(
                nx=nx, tf=tf, kernel=kernel, re=re,
                base_command=cmd
            ),
            f'isph_orig_shifting': dict(
                nx=nx, tf=tf, kernel=kernel, re=re,
                base_command=pysph_cmd, shift_freq=1,
                shift_kind='simple', scheme='isph'
            ),
            'sisph_gtvf': dict(
                nx=nx, tf=tf, kernel=kernel, base_command=cmd,
                tol=tol, gtvf=None, internal=None, no_symm=None
            ),
        }

        self.labels = {
            'sisph_no_gtvf': "Matrix - ISPH",
            'isph_orig_shifting': "Matrix - ISPH, Fickian smoothing",
            'sisph_gtvf': "SISPH, asymm",
        }

        self.cases = [
            TGV(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, label=self.labels[name], pfreq=10,
                **kwargs
            ) for name, kwargs in self.case_info.items()
        ]
        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_particles()

    def _plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        fig, ax = plt.subplots(1, 3, figsize=(8, 3), sharex=True, sharey=True)
        fig.subplots_adjust(wspace=0.2)
        ax = ax.T
        for i, case in enumerate(self.cases):
            files = get_files(case.input_path(), 'taylor_green')
            data = load(files[-1])
            f = data['arrays']['fluid']
            ax[i].scatter(
                f.x, f.y, marker='.', s=20, c='k', edgecolors='none',
                alpha=0.8, rasterized=False
            )
            # ax[i, j].set_xlim([0, 0.6])
            # ax[i, j].set_ylim([0.4, 1.0])
            ax[i].set_xlabel(self.labels[case.name])
        plt.tight_layout(pad=0)
        fig.savefig(self.output_path('pplots.pdf'))
        plt.close()


class FlowPastCylinder(Problem):
    def get_name(self):
        return 'flow_past_cylinder'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/flow_past_cylinder.py' + backend

        self.case_info = {
            'sisph_nx_30': (
                dict(base_command=cmd, nx=30),
                dict(scheme='sisph')
            ),
        }
        self.labels = {
            'sisph_nx_30': 'symm',
        }
        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **scheme_opts(self.case_info[name])
            ) for name in self.case_info
        ]
        merge_records(self.case_info)
        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._make_plot()
        self._particle_plots()

    def _make_plot(self):
        import matplotlib.pyplot as plt

        data = {}
        plt.figure(figsize=(6, 4))
        for scheme in self.case_info:
            data[scheme] = np.load(self.input_path(scheme, 'results.npz'))
            cd_avg = data[scheme]['cd'][-100:].mean()
            cl_avg = data[scheme]['cl'][-100:].max()
            print("Cd avg is ", cd_avg)
            print("Max Cl is ", cl_avg)
            plt.plot(data[scheme]['t'], data[scheme]['cl'],
                        label=r'$c_l$')
            plt.plot(data[scheme]['t'], data[scheme]['cd'],
                        label=r'$c_d$')
        plt.xlim(0, 200)
        plt.ylim(-1, 2)
        plt.xlabel(r'$t$')
        plt.legend(loc='best')
        plt.grid()
        plt.savefig(self.output_path('cl_cd.pdf'))
        plt.close()

    def _particle_plots(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        for case in self.cases:
            all_files = get_files(case.input_path(), 'flow_past_cylinder')
            times = [10, 153, 185]
            logfile = os.path.join(case.input_path(), 'flow_past_cylinder.log')
            files = get_files_at_given_times_from_log(all_files, times,
                                                      logfile)
            for i, file in enumerate(files):
                fig_p = plt.figure(figsize=(5, 4))
                fig_v = plt.figure(figsize=(5, 4))
                ax_p = fig_p.add_subplot(111)
                ax_v = fig_v.add_subplot(111)
                data = load(file)
                fluid = data['arrays']['fluid']
                solid = data['arrays']['solid']
                p, u, v, x, y = fluid.get('p', 'u', 'v', 'x', 'y')
                xs, ys = solid.get('x', 'y')
                vmag = np.sqrt(u*u + v*v)
                ps = ax_p.scatter(x, y, c=p, s=1, vmin=-1000, vmax=1000,
                                  cmap='seismic', rasterized=True)
                vs = ax_v.scatter(x, y, c=vmag, s=1, cmap='seismic',
                                  rasterized=True)
                ax_p.text(3, 12, f'T = {times[i]}s', fontsize=15)
                ax_v.text(3, 12, f'T = {times[i]}s', fontsize=15)
                ax_p.scatter(xs, ys, c='#440154FF', s=2)
                ax_v.scatter(xs, ys, c='#440154FF', s=2)
                fig_p.colorbar(ps, ax=ax_p)
                fig_v.colorbar(vs, ax=ax_v)
                ax_p.set_xlim(0, 30)
                ax_p.set_ylim(-15, 15)
                ax_v.set_xlim(0, 30)
                ax_v.set_ylim(-15, 15)
                fig_p.savefig(self.output_path(f"p_{i}.pdf"),
                              pad_inches=0)
                fig_v.savefig(self.output_path(f"vmag_{i}.pdf"),
                              pad_inches=0)

            plt.clf()
            data = load(all_files[0])
            solid = data['arrays']['solid']
            xs, ys = solid.get('x', 'y')
            fig, ax = plt.subplots()
            circle = plt.Circle((10, 0), 1, fc='gray', ec='k', zorder=0,
                                alpha=0.8)
            ax.add_artist(circle)
            ax.scatter(xs, ys, s=12, c='k', marker='o', zorder=1)
            plt.axis('equal')
            plt.axis('off')
            fig.savefig(self.output_path("initial_time.pdf"), pad_inches=0)
            plt.clf()



class DamBreak3DPerf(Problem):
    def get_name(self):
        return 'dam_break_3d_perf'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/dam_break_3d.py'
        tf = 0.05
        tol = 1e-2
        self.dx = dx = [0.2, 0.1, 0.04, 0.028, 0.015, 0.01]

        self._case_info = _case_info = {
            'sisph_perf_single_core': (
                dict(base_command=cmd, tol=tol, symmetric=None, gtvf=None),
                "Single Core", 'k-o',
            ),
            'sisph_perf_omp': (
                dict(base_command=cmd+' --openmp', tol=tol, symmetric=None,
                     gtvf=None),
                "OpenMP (4 threads)", 'b-o'
            ),
            'sisph_perf_gpu_1050Ti': (
                dict(base_command=cmd+' --opencl', tol=tol, symmetric=None,
                     gtvf=None),
                "GPU 1050Ti", 'g-o'
            ),
            'sisph_perf_gpu_1070Ti': (
                dict(base_command=cmd+' --opencl', tol=tol, symmetric=None,
                     gtvf=None),
                "GPU 1070Ti", 'r-o',
            ),
        }

        case_info = {
            f'{s}_dx_{d}': dict(
                dx=d, **_case_info[s][0]
            )
            for s, d in product(_case_info, dx)
        }

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, no_adaptive_timestep=None,
                disable_output=None,
                tf=tf,
                **scheme_opts(case_info[name])
            ) for name in case_info
        ]

    def run(self):
        self.make_output_dir()
        self.plot_perf()

    def plot_perf(self):
        import re
        import matplotlib.pyplot as plt
        t = {n: [] for n in self._case_info}
        for name in self._case_info:
            fname = []
            for x in self.cases:
                if name in x.name:
                    fname.append(x.name)
            for case in filter_by_name(self.cases, fname):
                with open(case.input_path('dam_break_3d.info'), 'r') as f:
                    data = json.load(f)
                with open(case.input_path('dam_break_3d.log'), 'r') as g:
                    nx = re.findall(r"Total:\ (\d+)", g.read())
                t[name].append((float(nx[0]), data['cpu_time']))
            plt.plot(*zip(*t[name]),
                     self._case_info[name][2], label=self._case_info[name][1])
        plt.xlabel("N (number of particles)")
        plt.ylabel("time (in secs)")
        plt.grid()
        plt.semilogx()
        plt.semilogy()
        plt.legend()
        plt.savefig(self.output_path("perf.pdf"), pad_inches=0)
        plt.clf()

        plt.figure()
        dx = [x for x, _ in sorted(t['sisph_perf_single_core'])]
        single_times = np.array([
            x for _, x in sorted(t['sisph_perf_single_core'])
        ])
        omp_times = np.array([
            x for _, x in sorted(t['sisph_perf_omp'])
        ])
        gpu50_times = np.array([
            x for _, x in sorted(t['sisph_perf_gpu_1050Ti'])
        ])
        gpu70_times = np.array([
            x for _, x in sorted(t['sisph_perf_gpu_1070Ti'])
        ])

        plt.plot(dx, single_times/omp_times, 'r-*',
                 label='Single core vs OpenMP (4 threads)')
        plt.plot(dx, single_times/gpu50_times, 'b-.',
                 label='Single core vs GPU 1050Ti')
        plt.plot(dx, single_times/gpu70_times, 'c-',
                 label='Single core vs GPU 1070Ti')
        plt.semilogx()
        plt.grid()
        plt.xlabel("N (number of particles)")
        plt.ylabel("speed up")
        plt.legend(loc='best')
        plt.savefig(self.output_path("speedup.pdf"), pad_inches=0)


class TaylorGreenPerf(Problem):
    def get_name(self):
        return 'taylor_green_perf'

    def setup(self):
        get_path = self.input_path

        pysph_cmd = 'pysph run taylor_green'
        cmd = 'python code/taylor_green.py'
        max_steps = 200
        tol = 1e-2
        self.nx = nx = [25, 50, 100, 200, 400, 500]

        self._case_info = _case_info = {
            'isph_shift_single_core': (
                dict(base_command=pysph_cmd, shift_freq=1,
                     shift_kind='simple', scheme='isph'),
                "ISPH, Single Core", '--*',
            ),
            'sisph_single_core': (
                dict(base_command=cmd, tol=tol, no_symmetric=None, gtvf=None),
                "SISPH, Single Core", '-*',
            ),
            'sisph_omp': (
                dict(base_command=cmd+' --openmp', tol=tol, no_symmetric=None,
                     gtvf=None),
                "SISPH, OpenMP (4 threads)", 'k-o'
            ),
        }

        case_info = {
            f'{s}_dx_{n}': dict(
                nx=n, **_case_info[s][0]
            )
            for s, n in product(_case_info, nx)
        }

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, no_adaptive_timestep=None,
                disable_output=None,
                max_steps=max_steps,
                **scheme_opts(case_info[name])
            ) for name in case_info
        ]

    def run(self):
        self.make_output_dir()
        self.plot_perf()

    def plot_perf(self):
        import re
        import matplotlib.pyplot as plt
        plt.figure()
        t = {n: [] for n in self._case_info}
        for name in self._case_info:
            fname = []
            for x in self.cases:
                if name in x.name:
                    fname.append(x.name)
            for case in filter_by_name(self.cases, fname):
                with open(case.input_path('taylor_green.info'), 'r') as f:
                    data = json.load(f)
                with open(case.input_path('taylor_green.log'), 'r') as g:
                    nx = re.findall(r"fluid:\ (\d+)", g.read())
                t[name].append((float(nx[0]), data['cpu_time']))
            plt.plot(*zip(*t[name]),
                     self._case_info[name][2], label=self._case_info[name][1])
        plt.xlabel("N (number of particles)")
        plt.ylabel("time (in secs)")
        plt.grid()
        plt.semilogx()
        plt.semilogy()
        plt.legend()
        plt.savefig(self.output_path("perf.pdf"), pad_inches=0)

        plt.figure()
        dx = [x for x, _ in sorted(t['sisph_single_core'])]
        isph_single = np.array([
            x for _, x in sorted(t['isph_shift_single_core'])
        ])
        sisph_single = np.array([
            x for _, x in sorted(t['sisph_single_core'])
        ])
        sisph_omp = np.array([
            x for _, x in sorted(t['sisph_omp'])
        ])

        plt.plot(dx, isph_single/sisph_single, 'r-*',
                 label='ISPH vs SISPH single core')
        plt.plot(dx, isph_single/sisph_omp, 'k-.',
                 label='ISPH, Single core vs SISPH, OpenMP')
        plt.semilogx()
        plt.grid()
        plt.xlabel("N (number of particles)")
        plt.ylabel("speed up")
        plt.legend(loc='best')
        plt.savefig(self.output_path("speedup.pdf"), pad_inches=0)
        plt.clf()


class EisphComparison(Problem):
    def get_name(self):
        return 'eisph_comparison'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/taylor_green.py'
        tol = 1e-3
        nx = 100
        re = 10000
        perturb = 0.1
        tf = 1.0

        self.case_info = _case_info = {
            'eisph': (
                dict(base_command=cmd+' --openmp', tol=tol, no_symmetric=None,
                     gtvf=None, max_iters=1),
                "EISPH", '-*',
            ),
            'sisph': (
                dict(base_command=cmd+' --openmp', tol=tol, no_symmetric=None,
                     gtvf=None, max_iters=100),
                "SISPH", 'k-o'
            ),
        }

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, no_adaptive_timestep=None,
                perturb=perturb, nx=nx, re=re, perf=None,
                tf=tf, **scheme_opts(self.case_info[name])
            ) for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.plot_particles()

    def plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        fig, ax = plt.subplots(1, 2, figsize=(10, 4), sharex=True, sharey=True)
        fig.subplots_adjust(wspace=0.2)
        for i, case in enumerate(self.cases):
            files = get_files(case.input_path(), 'taylor_green')

            data = load(files[-1])
            f = data['arrays']['fluid']
            tmp = ax[i].scatter(
                f.x, f.y, s=3, c=f.vmag, rasterized=True
            )
            ax[i].set_xlabel(self.case_info[case.name][1])
            ax[i].set_xlim(0, 1)
            ax[i].set_ylim(0, 1)
        fig.colorbar(tmp, ax=ax.tolist(), shrink=0.95, label='vmag')
        fig.savefig(self.output_path('comparison_vmag.pdf'))
        plt.close()


class DiffusionTest(Problem):
    def get_name(self):
        return 'diffusion_test'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/diffusion_test.py'
        tol = np.logspace(-1, -6, 6)
        self.tol = np.append(tol, 5*tol)
        nx = np.arange(20, 200, 20)
        self.dx = 1/nx
        hdx = 1.0

        self.case_info = {
            f'diffusion_tol_{t:.0e}_nx_{int(1/d)}': dict(
                base_command=cmd, tol=t, dx=d, max_iters=10000, hdx=hdx,
                perturb=0.0
            )
            for t, d in product(self.tol, self.dx)
        }

        self.case_info.update({
            f'diffusion_tol_{t}_nx_{int(1/d)}_perturb': dict(
                base_command=cmd, tol=t, dx=d, max_iters=10000, hdx=hdx,
                perturb=0.1
            )
            for t, d in product(self.tol, self.dx)
        })

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                **scheme_opts(self.case_info[name])
            ) for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.plots()

    def plots(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        for perturb in [0.0, 0.1]:
            cases = filter_cases(self.cases, perturb=perturb)
            for t in self.tol:
                err_mat = []
                err_jac = []
                for d in self.dx:
                    case = filter_cases(cases, tol=t, dx=d)
                    assert len(case) == 1
                    case = case[0]
                    files = get_files(case.input_path(), 'diffusion')

                    data = load(files[-1])
                    f = data['arrays']['fluid']

                    err_mat.append(np.linalg.norm(f.p_mat-f.p_ext))
                    err_jac.append(np.linalg.norm(f.p_mat-f.p_ext))
                plt.plot(self.dx, err_jac, '*', label=f'tol={t:.0e}')
                plt.plot(self.dx, err_mat, '*', label=f'tol={t:.0e}')
            plt.legend()
            plt.savefig(self.output_path('accuracy_jac_bicg.pdf'))
            plt.close()


class BCTest(Problem):
    def get_name(self):
        return 'bc_test'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/cavity.py'
        tol = 1e-3
        nx = 200
        re = 10000
        tf = 150
        pfreq = 250
        max_iters = 3000
        max_steps = 50000

        self.case_info = {
            f'cavity_noslip_bc': dict(
                base_command=cmd, bad_bc=None
            ),
            f'cavity_slip_bc': dict(
                base_command=cmd,
            )
        }

        self.label = {
            f'cavity_noslip_bc': r'No-slip velocity boundary \
            condition for $\bf{u}^{*}$',
            f'cavity_slip_bc': r'Slip velocity boundary condition \
            for $\bf{u}^{*}$'
        }

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                openmp=None, cache_nnps=None, symm=None, perf=None,
                tol=tol, nx=nx, max_iters=max_iters, max_steps=max_steps,
                gtvf=None, pfreq=pfreq, tf=tf, re=re,
                **scheme_opts(self.case_info[name])
            ) for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self.plot_particles()

    def plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files

        fig, ax = plt.subplots(1, 2, figsize=(11, 4), sharex=True, sharey=True)
        fig.subplots_adjust(wspace=0.2)
        for i, case in enumerate(self.cases):
            files = get_files(case.input_path(), 'cavity')

            data = load(files[-1])
            f = data['arrays']['fluid']
            tmp = ax[i].scatter(
                f.x, f.y, s=3, c=f.vmag, rasterized=True
            )
            ax[i].set_xlabel(self.label[case.name])
            ax[i].set_xlim(0, 1)
            ax[i].set_ylim(0, 1)
        fig.colorbar(tmp, ax=ax.tolist(), shrink=0.95, label='vmag')
        fig.savefig(self.output_path('good_vs_bad.pdf'),
                    bbox_inches='tight', pad_inches=0)
        plt.close()


class PlotIters(Problem):
    def get_name(self):
        return 'plot_iters'

    def setup(self):
        get_path = self.input_path

        cavity_cmd = 'python code/cavity.py'
        db2d_cmd = 'python code/dam_break_2d.py'
        sqp_cmd = 'python code/square_patch.py'
        self.tols = tols = [1e-2, 1e-3]
        nx = 50
        tf = 150
        pfreq = 250
        max_iters = 3000

        self.case_names = {
            'cavity': 'Cavity',
            'dam_break_2d': 'Dam-break 2D',
            # 'square_patch': 'Square patch'
        }

        _case_info = {
            f'cavity_jac': (dict(
                base_command=cavity_cmd, nx=100, pfreq=50, tf=5.0
            ), 'Cavity'),
            f'dam_break_2d_jac': (dict(
                base_command=db2d_cmd, pfreq=50, tf=1.0
            ), 'Dam-break 2D'),
            f'square_patch_jac': (dict(
                base_command=sqp_cmd, nx=100, scheme='sisph'
            ), 'Square patch'),
        }

        self.case_info = {
            f'{s}_tol_{tol}': dict(
                tol=tol, **_case_info[s][0]
            )
            for s, tol in product(_case_info, tols)
        }

        self.case_label = {
            f'{s}_tol_{tol}': _case_info[s][1]
            for s, tol in product(_case_info, tols)
        }

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                openmp=None, cache_nnps=None, symm=None,
                gtvf=None, max_iters=max_iters, perf=None,
                **scheme_opts(self.case_info[name])
            ) for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self._make_table()

    def _make_table(self):

        column_names = ['Problem', 'Tolerance',
                        'Jacobi (avg no of. iterations)']
        rows = []
        for ci, problem in self.case_names.items():
            for tol in self.tols:
                tol = str(tol)
                row = [problem, tol, _get_iters(ci, tol, self.case_info,
                                                self.cases)]
                rows.append(row)
        make_table(
            column_names, rows, self.output_path(self.get_name() + '_avg.tex'),
            sort_cols=[1, -1], column_format='lrr'
        )


def _get_iters(ci, tol, case_info, case_data):
    names = [x for x in case_info if ci in x and tol in x]
    cases = filter_by_name(case_data, names)
    for case in cases:
        iters = []
        files = get_files(case.input_path(), ci)
        for file in files:
            data = load(file)
            f = data['arrays']['fluid']
            iters.append(f.iters[0])
            jacobi_iters = iters.copy()
    return np.ceil(np.mean(jacobi_iters))


if __name__ == '__main__':
    PROBLEMS = [
        SquarePatch,
        DamBreak2D,
        DamBreak3D,
        Cavity,
        TaylorGreen,
        FlowPastCylinder,
        DamBreak3DPerf,
        TaylorGreenPerf,
        TaylorGreenParticlePlots,
        EisphComparison,
        BCTest,
        PlotIters
    ]
    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=PROBLEMS
    )
    automator.run()
